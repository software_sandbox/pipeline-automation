package com.tmdei.core.kafka;

import com.tmdei.core.services.JenkinsService;
import com.tmdei.jenkins.domain.CreateJobPayload;
import com.tmdei.jenkins.domain.dto.JenkinsConfig;
import com.tmdei.jenkins.domain.dto.JobDto;
import com.tmdei.jenkins.domain.dto.SeedDto;
import com.tmdei.utils.exceptions.AutomationException;
import com.tmdei.utils.http.exceptions.HttpException;
import com.tmdei.utils.mongo.exceptions.MongoException;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.sql.Timestamp; 

import static com.tmdei.core.kafka.config.KafkaTopicConfig.JENKINS_TOPIC;
import static com.tmdei.core.utils.Variables.DEFAULT_SEED_JOB;

@Component
public class JenkinsKafkaConsumer {

    private final Logger LOGGER = LoggerFactory.getLogger(JenkinsKafkaConsumer.class);

    private final JenkinsService jenkinsService;


    public JenkinsKafkaConsumer(JenkinsService jenkinsService) {
        this.jenkinsService = jenkinsService;
    }

    @KafkaListener(topics = JENKINS_TOPIC, groupId = "jenkins.consumer")
    public void consume(ConsumerRecord<Long, JenkinsConfig> record) {
        if (record.value() != null) {
            LOGGER.info("KAFKA >> Received new Jenkins Configuration {}", record.key());
            try {
                consumeRecord(record);
            } catch (Exception e) {
                LOGGER.error("KAFKA >> Error Processing Record");
                throw new RuntimeException(e);
            }
        } else {
            LOGGER.error("KAFKA >> Invalid Record: null");
        }
    }

    private void consumeRecord(ConsumerRecord<Long, JenkinsConfig> record) {
		LOGGER.info("JENKINS >> Starting Configuration {}", new Timestamp(System.currentTimeMillis()));
        JenkinsConfig validatedPayload = validatePayload(record.value());
        dispatchSeeds(validatedPayload.getSeeds());
        dispatchJobs(validatedPayload.getJobs());
        dispatchTriggers(validatedPayload.getTrigger());
		LOGGER.info("JENKINS >> Configuration Finished {}", new Timestamp(System.currentTimeMillis()));
    }

    private JenkinsConfig validatePayload(JenkinsConfig payload) {
        return JenkinsConfig.builder()
                .jobs(Objects.nonNull(payload.getJobs()) ? payload.getJobs() : Collections.emptyList())
                .trigger(Objects.nonNull(payload.getTrigger()) ? payload.getTrigger() : Collections.emptyList())
                .seeds(Objects.nonNull(payload.getSeeds()) ? payload.getSeeds() : Collections.emptyList())
                .build();
    }

    private void dispatchJobs(List<JobDto> jobs) {
        jobs.forEach(jobDto -> {
            try {
                jenkinsService.triggerJenkinsSeedJob(
                        Objects.isNull(jobDto.getSeedTemplate()) ? DEFAULT_SEED_JOB : jobDto.getSeedTemplate(),
                        CreateJobPayload.builder()
                                .jobName(jobDto.getJobName())
                                .repoUrl(jobDto.getRepoUrl())
                                .build());
                LOGGER.info("Successfully created job {}",
                        jobDto.getJobName());
            } catch (HttpException | AutomationException | MongoException e) {
                LOGGER.error("ERROR >> Failed to create job: {}", jobDto.getJobName());
            }
        });
    }

    private void dispatchSeeds(List<SeedDto> seeds) {
        seeds.forEach(seedDto -> {
            try {
                jenkinsService.createJenkinsSeedJob(
                        seedDto.getSeedName(),
                        seedDto.getSeedTemplate().getBytes(StandardCharsets.UTF_8)
                );
                LOGGER.info("Successfully created seed job {}",
                        seedDto.getSeedName());
            } catch (HttpException | AutomationException | MongoException e) {
                LOGGER.error("ERROR >> Failed to create seed job: {}", seedDto.getSeedName());
            }
        });
    }

    private void dispatchTriggers(List<String> triggers) {
        triggers.forEach(trigger -> {
            try {
                jenkinsService.triggerJenkinsJob(
                        trigger
                );
                LOGGER.info("Successfully triggered job {}",
                        trigger);
            } catch (HttpException | AutomationException e) {
                LOGGER.error("ERROR >> Failed to trigger job: {}", trigger);
            }
        });
    }
}
