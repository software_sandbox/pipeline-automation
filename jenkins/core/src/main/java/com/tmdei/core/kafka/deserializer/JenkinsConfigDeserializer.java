package com.tmdei.core.kafka.deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmdei.jenkins.domain.dto.JenkinsConfig;
import org.apache.kafka.common.serialization.Deserializer;

import java.nio.charset.StandardCharsets;

public class JenkinsConfigDeserializer implements Deserializer<JenkinsConfig> {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public JenkinsConfig deserialize(String s, byte[] bytes) {
        try {
            if (bytes == null) {
                System.out.println("Null received at deserializing");
                return null;
            }
            System.out.println("Deserializing...");
            return objectMapper.readValue(new String(bytes, StandardCharsets.UTF_8), JenkinsConfig.class);
        } catch (Exception e) {
            System.out.println("Error when deserializing byte[] to a valid Jenkins Configuration");
            return null;
        }
    }
}
