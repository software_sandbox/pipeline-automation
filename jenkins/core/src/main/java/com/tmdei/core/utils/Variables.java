package com.tmdei.core.utils;

public class Variables {

    public static final String JOBS_PATH = "createItem?name=";

    public static final String BUILD = "/build";

    public static final String JOB = "/job/";

    public static final String TRIGGER_SEED = "/buildWithParameters";

    public static final String JOB_PARAM = "job=";

    public static final String URL_PARAM = "url=";

    public static final String DEFAULT_JENKINS_TEMPLATE = "config.xml";

    public static final String DEFAULT_JENKINS_SEED_TEMPLATE = "seedJobConfig.xml";

    public static final String USER_DIR = "user.dir";

    public static final String CONCAT_URL_PARAMS = "&";

    public static final String ADD_URL_PARAMS = "?";

    public static String XML_URL_NODE = "hudson.plugins.git.UserRemoteConfig";

    public static String XML_DSL_NODE = "javaposse.jobdsl.plugin.ExecuteDslScripts";

    public static String SCRIPT_TEXT = "scriptText";

    public static String URL = "url";

    public static String JOB_STATUS = "api/json?tree=jobs[name,color]";

    public static String JOBS = "jobs";

    public static String BLUE = "blue";

    public static String DEFAULT_SEED_JOB = "seedTemplate";

}
