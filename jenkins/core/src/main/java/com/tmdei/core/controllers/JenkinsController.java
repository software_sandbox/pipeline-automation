package com.tmdei.core.controllers;

import com.tmdei.core.services.JenkinsService;
import com.tmdei.jenkins.domain.CreateJobPayload;
import com.tmdei.jenkins.domain.JobStatusInfo;
import com.tmdei.utils.exceptions.AutomationException;
import com.tmdei.utils.http.exceptions.HttpException;
import com.tmdei.utils.mongo.exceptions.MongoException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

@RestController
@RequestMapping(path = "/jenkins")
public class JenkinsController {

    private final JenkinsService jenkinsService;


    public JenkinsController(JenkinsService jenkinsService) {
        this.jenkinsService = jenkinsService;
    }


    @PostMapping(path = "/create/job")
    public ResponseEntity<String> createJenkinsJob(@RequestBody CreateJobPayload payload) {
        try {
            String response = jenkinsService.createJenkinsJob(payload.getRepoUrl(), payload.getJobName());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        }
    }

    @PostMapping(path = "/create/seed/{seedName}")
    public ResponseEntity<String> createJenkinsSeedJob(@PathVariable String seedName, @RequestBody byte[] payload) {
        try {
            String response = jenkinsService.createJenkinsSeedJob(seedName, payload);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        } catch (MongoException e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }

    @PostMapping(path = "/create/seed/encoded")
    public ResponseEntity<String> createJenkinsSeedJobEncodedData(@RequestBody CreateJobPayload payload) {
        try {
            String response = jenkinsService.createJenkinsSeedJob(payload.getJobName(), payload.getJobTemplate().getBytes(StandardCharsets.UTF_8));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        } catch (MongoException e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }

    @PostMapping(path = "/trigger/{jobName}")
    public ResponseEntity<String> triggerJenkinsJob(@PathVariable String jobName) {
        try {
            String response = jenkinsService.triggerJenkinsJob(jobName);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        }
    }

    @PostMapping(path = "/trigger/seed/{seedName}")
    public ResponseEntity<String> triggerJenkinsSeedJob(@PathVariable String seedName,
                                                        @RequestBody CreateJobPayload createJobPayload) {
        try {
            String response = jenkinsService.triggerJenkinsSeedJob(seedName, createJobPayload);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        } catch (MongoException e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }

    @GetMapping(path = "/status")
    public ResponseEntity<List<JobStatusInfo>> getJobStatus() {
        try {
            List<JobStatusInfo> response = jenkinsService.getJobStatus();
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        } catch (IOException ioE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()), ioE.getMessage(), ioE);
        }
    }

}
