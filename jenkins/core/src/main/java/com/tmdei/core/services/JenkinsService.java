package com.tmdei.core.services;

import com.tmdei.jenkins.domain.CreateJobPayload;
import com.tmdei.jenkins.domain.JenkinsDomain;
import com.tmdei.jenkins.domain.JobStatusInfo;
import com.tmdei.jenkins.domain.database.JobDbo;
import com.tmdei.utils.exceptions.AutomationException;
import com.tmdei.utils.http.DefaultHttpClient;
import com.tmdei.utils.http.exceptions.HttpException;
import com.tmdei.utils.mapper.GenericMapper;
import com.tmdei.utils.mongo.MongoUtils;
import com.tmdei.utils.mongo.exceptions.MongoException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import static com.tmdei.core.utils.Variables.ADD_URL_PARAMS;
import static com.tmdei.core.utils.Variables.BUILD;
import static com.tmdei.core.utils.Variables.CONCAT_URL_PARAMS;
import static com.tmdei.core.utils.Variables.DEFAULT_JENKINS_SEED_TEMPLATE;
import static com.tmdei.core.utils.Variables.DEFAULT_JENKINS_TEMPLATE;
import static com.tmdei.core.utils.Variables.JOB;
import static com.tmdei.core.utils.Variables.JOBS;
import static com.tmdei.core.utils.Variables.JOBS_PATH;
import static com.tmdei.core.utils.Variables.JOB_PARAM;
import static com.tmdei.core.utils.Variables.JOB_STATUS;
import static com.tmdei.core.utils.Variables.SCRIPT_TEXT;
import static com.tmdei.core.utils.Variables.TRIGGER_SEED;
import static com.tmdei.core.utils.Variables.URL;
import static com.tmdei.core.utils.Variables.URL_PARAM;
import static com.tmdei.core.utils.Variables.XML_DSL_NODE;
import static com.tmdei.core.utils.Variables.XML_URL_NODE;
import static com.tmdei.utils.utils.MiscUtils.checkThrow;
import static com.tmdei.utils.utils.MiscUtils.concatUrl;
import static com.tmdei.utils.utils.MiscUtils.handleResponseStatusCode;
import static com.tmdei.utils.utils.MiscUtils.xmlFileReplaceValue;

@Service
public class JenkinsService {

    private final String url;

    private final DefaultHttpClient<Object> jenkinsHttpClient;

    private final GenericMapper<JobStatusInfo> mapper;

    private final MongoUtils<JobDbo> mongoUtilsJobs;

    @Qualifier("mongoTemplateRepository")
    private final MongoTemplate mongoTemplateJenkins;

    public JenkinsService(DefaultHttpClient<Object> jenkinsHttpClient, @Value("${jenkins.api.default.url}") String url,
                          GenericMapper<JobStatusInfo> mapper,
                          MongoTemplate mongoTemplateJenkins,
                          MongoUtils<JobDbo> mongoUtilsJobs) {
        this.jenkinsHttpClient = jenkinsHttpClient;
        this.url = url;
        this.mapper = mapper;
        this.mongoUtilsJobs = mongoUtilsJobs;
        this.mongoTemplateJenkins = mongoTemplateJenkins;
    }

    public String createJenkinsJob(String repoUrl, String jobName) throws AutomationException, HttpException {
        String defaultJenkinsJobFilePath = Objects.requireNonNull(this.getClass().getClassLoader().getResource(DEFAULT_JENKINS_TEMPLATE)).toExternalForm();
        return jenkinsPostFile(defaultJenkinsJobFilePath, xmlFileReplaceValue(defaultJenkinsJobFilePath, XML_URL_NODE, URL, repoUrl),
                url, JOBS_PATH, jobName);
    }

    public String createJenkinsSeedJob(String jobName, byte[] template) throws AutomationException, HttpException, MongoException {
        String defaultJenkinsSeedJobFilePath = Objects.requireNonNull(this.getClass().getClassLoader().getResource(DEFAULT_JENKINS_SEED_TEMPLATE)).toExternalForm();
        String handledResponse = jenkinsPostFile(defaultJenkinsSeedJobFilePath, template,
                url, JOBS_PATH, jobName);
        mongoUtilsJobs.insertRecord(new JobDbo(jobName, null, new String(template, StandardCharsets.UTF_8)),
                mongoTemplateJenkins);
        return handledResponse;
    }

    public String triggerJenkinsJob(String jobName) throws AutomationException, HttpException {
        return jenkinsPost(null, url, JOB, jobName, BUILD);
    }

    public String triggerJenkinsSeedJob(String seedName, CreateJobPayload createJobPayload) throws AutomationException, HttpException, MongoException {
        String handledResponse = jenkinsPost(null, url, JOB, seedName, TRIGGER_SEED, ADD_URL_PARAMS, JOB_PARAM, createJobPayload.getJobName(), CONCAT_URL_PARAMS,
                URL_PARAM, createJobPayload.getRepoUrl());
        mongoUtilsJobs.insertRecord(new JobDbo().map(createJobPayload), mongoTemplateJenkins);
        return handledResponse;
    }

    public List<JobStatusInfo> getJobStatus() throws IOException, AutomationException, HttpException {
        return sortByStatus(
                this.mapper.parseResponseBodyList(
                        this.mapper.getMapper().readTree(jenkinsGet(url, JOB_STATUS)).get(JOBS).toPrettyString(), JobStatusInfo.class));
    }

    private List<JobStatusInfo> sortByStatus(List<JobStatusInfo> jobsStatus) {
        jobsStatus.sort(Comparator.comparing(JobStatusInfo::getColor));
        return jobsStatus;
    }

    /********************************************************************************************************************************************************************************
     *
     * GENERIC METHODS
     *
     *******************************************************************************************************************************************************************************/
    private String jenkinsGet(Object... strings) throws HttpException, AutomationException {
        HttpResponse<String> response = this.jenkinsHttpClient.genericGetRequest(
                concatUrl(strings));
        String handledResponse = handleResponseStatusCode(response);
        checkThrow(response, handledResponse);
        return handledResponse;
    }

    private String jenkinsPost(JenkinsDomain body, Object... urlParams) throws HttpException, AutomationException {
        HttpResponse<String> response = this.jenkinsHttpClient.genericPostRequest(
                concatUrl(urlParams),
                body);
        String handledResponse = handleResponseStatusCode(response);
        checkThrow(response, handledResponse);
        return handledResponse;
    }

    private String jenkinsPostFile(String path, byte[] template, Object... urlParams) throws HttpException, AutomationException {
        HttpResponse<String> response = this.jenkinsHttpClient.genericPostRequestFile(
                concatUrl(urlParams),
                xmlFileReplaceValue(path.split(":")[1], XML_DSL_NODE, SCRIPT_TEXT, new String(template, StandardCharsets.UTF_8)));
        String handledResponse = handleResponseStatusCode(response);
        checkThrow(response, handledResponse);
        return handledResponse;
    }

}
