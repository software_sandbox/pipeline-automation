package com.tmdei.jenkins.domain.database;

import com.tmdei.jenkins.domain.CreateJobPayload;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document("jobs.data")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobDbo {

    @Id
    private String jobName;

    private String repoUrl;

    private String jobTemplate;

    public JobDbo map(CreateJobPayload payload) {
        return new JobDbo(
                Objects.isNull(payload.getJobName()) ? null : payload.getJobName(),
                Objects.isNull(payload.getRepoUrl()) ? null : payload.getRepoUrl(),
                Objects.isNull(payload.getJobTemplate()) ? null : payload.getJobTemplate()
        );
    }
}
