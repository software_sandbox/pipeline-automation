package com.tmdei.jenkins.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JenkinsConfig {

    private List<JobDto> jobs;

    private List<String> trigger;

    private List<SeedDto> seeds;
}
