package com.tmdei.jenkins.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JobDto {

    private String seedTemplate = "seedJob";

    private String repoUrl;

    private String jobName;


}
