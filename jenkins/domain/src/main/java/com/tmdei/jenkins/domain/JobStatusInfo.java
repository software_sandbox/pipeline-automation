package com.tmdei.jenkins.domain;

import com.tmdei.jenkins.domain.enums.JobStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JobStatusInfo implements JenkinsDomain {

    private String name;

    private JobStatus color;

}
