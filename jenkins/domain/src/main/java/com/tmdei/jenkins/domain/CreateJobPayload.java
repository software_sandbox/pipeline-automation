package com.tmdei.jenkins.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateJobPayload implements JenkinsDomain {

    private String jobName;

    private String repoUrl;

    private String jobTemplate;

}