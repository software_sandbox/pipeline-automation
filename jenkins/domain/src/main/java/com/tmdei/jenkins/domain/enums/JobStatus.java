package com.tmdei.jenkins.domain.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum JobStatus {
    PASSING("passing"),
    FAILING("failing"),
	NOTBUILT("notbuilt");

    private static final String BLUE = "blue";
	
	private static final String RED = "red";
	
	 private static final String BLUE_PASSING = "passing";
	
	private static final String RED_FAILING = "failing";
	
    public final String color;

    private JobStatus(String color) {
        this.color = color;
    }

    @JsonCreator
    public static JobStatus forValues(String color) {
		JobStatus result = null;
		switch (color) {
			case BLUE:
			case BLUE_PASSING:
				result = JobStatus.PASSING;
				break;
			case RED:
			case RED_FAILING:
				result = JobStatus.FAILING;
				break;
			default:
				result = JobStatus.NOTBUILT;
				break;
		}
		return result;
	}

    @JsonValue
    String getStatus() {
        return color;
    }

}
