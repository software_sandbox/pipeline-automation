package com.tmdei.sonar.domain.dto;

import com.tmdei.sonar.domain.SonarDomain;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SonarConfig implements SonarDomain {

    private List<ProjectDto> sonarProjects;

    private List<UserDto> sonarUsers;

    private List<PermissionsDto> sonarPermissions;

    private ModificationsDto modifications;

}
