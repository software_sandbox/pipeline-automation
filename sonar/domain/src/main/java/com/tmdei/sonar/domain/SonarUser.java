package com.tmdei.sonar.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SonarUser implements SonarDomain {

    private String login;

    private String email;

    private String name;

    private boolean local;

}
