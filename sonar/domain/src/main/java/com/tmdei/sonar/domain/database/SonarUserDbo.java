package com.tmdei.sonar.domain.database;


import com.tmdei.sonar.domain.SonarUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document("users.data")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SonarUserDbo {

    @Id
    private String login;

    private String email;

    private String name;

    private boolean local;


    public SonarUserDbo map(SonarUser sonarUser) {
        return new SonarUserDbo(
                Objects.isNull(sonarUser.getLogin()) ? null : sonarUser.getLogin(),
                Objects.isNull(sonarUser.getEmail()) ? null : sonarUser.getEmail(),
                Objects.isNull(sonarUser.getName()) ? null : sonarUser.getName(),
                sonarUser.isLocal()
        );
    }

}
