package com.tmdei.sonar.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ModificationsDto {

    private List<MoveUsers> moveUsers;

    private List<RemoveUsers> removeUsers;

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class MoveUsers {

        private String login;

        private String projectNameSource;

        private String projectNameTarget;

        private String permission;

    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class RemoveUsers {

        private String login;

        private List<String> projects;


    }

}
