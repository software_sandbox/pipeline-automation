package com.tmdei.sonar.domain;

import com.tmdei.sonar.domain.enums.VisibilityLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SonarProject implements SonarDomain {

    private String name;

    private String key;

    private VisibilityLevel visibility;

}
