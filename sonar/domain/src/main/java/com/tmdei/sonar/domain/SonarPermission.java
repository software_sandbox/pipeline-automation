package com.tmdei.sonar.domain;

import com.tmdei.sonar.domain.enums.PermissionLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SonarPermission implements SonarDomain {

    private String login;

    private PermissionLevel permission;

    private String projectKey;

}
