package com.tmdei.sonar.domain.database;

import com.tmdei.sonar.domain.SonarProject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document("projects.data")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SonarProjectDbo {

    @Id
    private String name;

    private String key;

    private String visibility;


    public SonarProjectDbo map(SonarProject sonarProject) {
        return new SonarProjectDbo(
                Objects.isNull(sonarProject.getName()) ? null : sonarProject.getName(),
                Objects.isNull(sonarProject.getKey()) ? null : sonarProject.getKey(),
                Objects.isNull(sonarProject.getVisibility()) ? null : sonarProject.getVisibility().visibility
        );
    }

}
