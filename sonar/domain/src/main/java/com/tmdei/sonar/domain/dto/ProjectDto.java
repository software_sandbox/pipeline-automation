package com.tmdei.sonar.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProjectDto {

    private String name;

    private String key;

    private String visibility;

    private List<PermissionsDto> projectsUsers;

}

