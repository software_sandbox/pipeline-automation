package com.tmdei.sonar.domain.mapper;

import com.tmdei.sonar.domain.SonarProject;
import com.tmdei.sonar.domain.SonarUser;
import com.tmdei.sonar.domain.dto.ProjectDto;
import com.tmdei.sonar.domain.dto.UserDto;
import com.tmdei.sonar.domain.enums.VisibilityLevel;

public class DtoToDomainMapper {

    public static SonarProject mapProject(ProjectDto projectDto) {
        return SonarProject.builder()
                .name(projectDto.getName())
                .key(projectDto.getKey())
                .visibility(VisibilityLevel.valueOf(projectDto.getVisibility().toUpperCase()))
                .build();
    }

    public static SonarUser mapUsers(UserDto userDto) {
        return SonarUser.builder()
                .name(userDto.getName())
                .login(userDto.getLogin())
                .email(userDto.getEmail())
                .local(false)
                .build();
    }

}
