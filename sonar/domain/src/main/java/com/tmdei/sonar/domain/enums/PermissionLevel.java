package com.tmdei.sonar.domain.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum PermissionLevel {

    @JsonProperty("admin")
    ADMIN("admin"),

    @JsonProperty("codeviewer")
    CODEVIEWER("codeviewer"),

    @JsonProperty("issueadmin")
    ISSUEADMIN("issueadmin"),

    @JsonProperty("securityhotspotadmin")
    SECURITYHOTSPOTADMIN("securityhotspotadmin"),

    @JsonProperty("scan")
    SCAN("scan"),

    @JsonProperty("user")
    USER("user");

    public final String permission;

    private PermissionLevel(String permission) {
        this.permission = permission;
    }

}

