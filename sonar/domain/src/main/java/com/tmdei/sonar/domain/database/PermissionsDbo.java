package com.tmdei.sonar.domain.database;

import com.tmdei.sonar.domain.SonarPermission;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Objects;

@Document("permissions.data")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PermissionsDbo {

    @Id
    private CompositeKey id;

    private String login;

    private String permission;

    private String projectKey;

    public PermissionsDbo map(SonarPermission sonarPermission) {
        return new PermissionsDbo(
                new CompositeKey(sonarPermission.getLogin(),
                        sonarPermission.getPermission().permission,
                        sonarPermission.getProjectKey()),
                Objects.isNull(sonarPermission.getLogin()) ? null : sonarPermission.getLogin(),
                Objects.isNull(sonarPermission.getPermission()) ? null : sonarPermission.getPermission().permission,
                Objects.isNull(sonarPermission.getProjectKey()) ? null : sonarPermission.getProjectKey()
        );
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class CompositeKey implements Serializable {
        private String login;

        private String permission;

        private String projectKey;
    }

}
