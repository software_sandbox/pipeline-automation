package com.tmdei.sonar.domain.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum VisibilityLevel {
    @JsonProperty("private")
    PRIVATE("private"),

    @JsonProperty("public")
    PUBLIC("public");

    public final String visibility;

    private VisibilityLevel(String visibility) {
        this.visibility = visibility;
    }
}
