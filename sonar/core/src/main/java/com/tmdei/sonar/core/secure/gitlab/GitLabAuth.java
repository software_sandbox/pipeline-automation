package com.tmdei.sonar.core.secure.gitlab;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class GitLabAuth {

    private final String token;

    private final String authorization;

    public GitLabAuth(@Value("${gitlab.api.token}") String token,
                      @Value("${gitlab.api.auth}") String authorization) {
        this.token = token;
        this.authorization = authorization;
    }

    public Map<String, String> loadAuthHeaders() {
        return Stream.of(new AbstractMap.SimpleEntry<>(this.authorization, this.token))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

}
