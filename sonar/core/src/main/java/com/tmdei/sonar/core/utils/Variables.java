package com.tmdei.sonar.core.utils;

public class Variables {

    public static final String UPDATE_USER_AUTH_PATH = "api/users/update_identity_provider";

    public static final String EXTERNAL_PROVIDER = "newExternalProvider=";

    public static final String EXTERNAL_IDENTITY = "newExternalIdentity=";

    public final static String GITLAB = "gitlab";

    public static final String NAME_URL = "name=";

    public static final String LOGIN_URL = "login=";

    public static final String PROJECT_KEY = "projectKey=";

    public static final String PERMISSION = "permission=";

    public static final String LOCAL_URL = "local=";

    public static final String EMAIL_URL = "email=";

    public static final String CREATE_USERS_PATH = "api/users/create";

    public static final String SEARCH_BY_PROJECT_STATUS = "?filter=alert_status=";

    public static final String SEARCH_PROJECTS = "api/components/search_projects";

    public static final String OK = "OK";

    public static final String ERROR = "ERROR";

    public static final String ADD_PERMISSION_PATH = "api/permissions/add_user";

    public static final String REMOVE_PERMISSION_PATH = "api/permissions/remove_user";

    public static final String TOKEN_URL = "project=";

    public static final String VISIBILITY_URL = "visibility=";

    public static final String CONCAT_URL_PARAMS = "&";

    public static final String ADD_URL_PARAMS = "?";

    public static final String CREATE_PATH = "api/projects/create";

    public static final String PROJECT = "project";

    public static final String USER = "user";

    public static final String GITLAB_SEARCH = "users?search=";

    public static final String APPLICATION_JSON = "application/json";

}
