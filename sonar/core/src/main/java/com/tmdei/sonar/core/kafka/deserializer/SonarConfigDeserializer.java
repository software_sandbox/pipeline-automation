package com.tmdei.sonar.core.kafka.deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmdei.sonar.domain.dto.SonarConfig;
import org.apache.kafka.common.serialization.Deserializer;

import java.nio.charset.StandardCharsets;

public class SonarConfigDeserializer implements Deserializer<SonarConfig> {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public SonarConfig deserialize(String s, byte[] bytes) {
        try {
            if (bytes == null) {
                System.out.println("Null received at deserializing");
                return null;
            }
            System.out.println("Deserializing...");
            return objectMapper.readValue(new String(bytes, StandardCharsets.UTF_8), SonarConfig.class);
        } catch (Exception e) {
            System.out.println("Error when deserializing byte[] to a valid Sonar Configuration");
            return null;
        }
    }
}
