package com.tmdei.sonar.core.secure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
public class AuthConfig {

    private final BasicAuth basicAuth;

    public AuthConfig(BasicAuth basicAuth) {
        this.basicAuth = basicAuth;
    }

    @Bean
    public Map<String, String> headers() {
        return basicAuth.loadAuthHeaders();
    }
}
