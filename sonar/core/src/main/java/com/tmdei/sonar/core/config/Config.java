package com.tmdei.sonar.core.config;

import com.tmdei.utils.http.config.HttpConfig;
import com.tmdei.utils.kafka.config.KafkaAdminConfig;
import com.tmdei.utils.mapper.MapperConfig;
import com.tmdei.utils.mongo.config.MongoConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(
        {
                HttpConfig.class,
                MapperConfig.class,
                MongoConfig.class,
                KafkaAdminConfig.class
        }
)
public class Config {
}