package com.tmdei.sonar.core.controllers;

import com.tmdei.sonar.core.services.SonarService;
import com.tmdei.sonar.domain.SonarPermission;
import com.tmdei.sonar.domain.SonarProject;
import com.tmdei.sonar.domain.SonarUser;
import com.tmdei.utils.exceptions.AutomationException;
import com.tmdei.utils.http.exceptions.HttpException;
import com.tmdei.utils.mongo.exceptions.MongoException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static com.tmdei.sonar.core.utils.Variables.APPLICATION_JSON;

@RestController
@RequestMapping(path = "/sonar", produces = APPLICATION_JSON)
public class SonarController {

    private final SonarService sonarService;

    public SonarController(SonarService sonarService) {
        this.sonarService = sonarService;
    }

    @PostMapping(path = "/create/project")
    public ResponseEntity<SonarProject> createSonarProject(@RequestBody SonarProject payload) {
        try {
            return new ResponseEntity<>(sonarService.createSonarProject(payload), HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        } catch (MongoException e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }

    @PostMapping(path = "/create/user")
    public ResponseEntity<SonarUser> createSonarUser(@RequestBody SonarUser payload) {
        try {
            return new ResponseEntity<>(sonarService.createSonarUser(payload), HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        } catch (MongoException e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }

    @PostMapping(path = "/permission/add")
    public ResponseEntity<String> addPermissionToUser(@RequestBody SonarPermission payload) {
        try {
            sonarService.addPermissionToUser(payload);
            return new ResponseEntity<>(String.format("{\"message\": \"Permission %s added to User %s\"}",
                    payload.getPermission(), payload.getLogin()), HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        } catch (MongoException e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }

    @PostMapping(path = "/permission/remove")
    public ResponseEntity<String> removePermissionFromUser(@RequestBody SonarPermission payload) {
        try {
            sonarService.removePermissionFromUser(payload);
            return new ResponseEntity<>(String.format("{\"message\": \"Permission %s removed from User %s\"}",
                    payload.getPermission(), payload.getLogin()), HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        } catch (MongoException e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }

    @GetMapping(path = "/projects/passing")
    public ResponseEntity<List<SonarProject>> getPassingProjects() {
        try {
            return new ResponseEntity<>(sonarService.getPassingProjects(), HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        }
    }

    @GetMapping(path = "/projects/failing")
    public ResponseEntity<List<SonarProject>> getFailingProjects() {
        try {
            return new ResponseEntity<>(sonarService.getFailingProjects(), HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        }
    }

    @GetMapping(path = "/projects/all")
    public ResponseEntity<List<SonarProject>> getAllProjects() {
        try {
            return new ResponseEntity<>(sonarService.getAllProjects(), HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        }
    }
}
