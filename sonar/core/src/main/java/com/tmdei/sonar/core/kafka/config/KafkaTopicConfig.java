package com.tmdei.sonar.core.kafka.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaTopicConfig {

    public static final String SONAR_TOPIC = "sonar.config";

    public static final String SONAR_GROUP_ID = "sonar.consumer";

    private static final Integer NUM_PARTITIONS = 5;

    private static final Short REPLICATION_FACTOR = 1;

    @Bean
    public NewTopic repository() {
        return new NewTopic(SONAR_TOPIC, NUM_PARTITIONS, REPLICATION_FACTOR);
    }
}
