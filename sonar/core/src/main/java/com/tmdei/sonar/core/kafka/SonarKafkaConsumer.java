package com.tmdei.sonar.core.kafka;

import com.tmdei.sonar.core.services.SonarService;
import com.tmdei.sonar.domain.SonarPermission;
import com.tmdei.sonar.domain.SonarProject;
import com.tmdei.sonar.domain.database.PermissionsDbo;
import com.tmdei.sonar.domain.dto.ModificationsDto;
import com.tmdei.sonar.domain.dto.ModificationsDto.MoveUsers;
import com.tmdei.sonar.domain.dto.ModificationsDto.RemoveUsers;
import com.tmdei.sonar.domain.dto.PermissionsDto;
import com.tmdei.sonar.domain.dto.ProjectDto;
import com.tmdei.sonar.domain.dto.SonarConfig;
import com.tmdei.sonar.domain.dto.UserDto;
import com.tmdei.sonar.domain.enums.PermissionLevel;
import com.tmdei.sonar.domain.enums.VisibilityLevel;
import com.tmdei.sonar.domain.mapper.DtoToDomainMapper;
import com.tmdei.utils.exceptions.AutomationException;
import com.tmdei.utils.http.exceptions.HttpException;
import com.tmdei.utils.mongo.MongoUtils;
import com.tmdei.utils.mongo.exceptions.MongoException;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.sql.Timestamp; 

import static com.tmdei.sonar.core.kafka.config.KafkaTopicConfig.SONAR_TOPIC;

@Component
public class SonarKafkaConsumer {

    private final Logger LOGGER = LoggerFactory.getLogger(SonarKafkaConsumer.class);

    private final SonarService sonarService;

    private final MongoUtils<PermissionsDbo> mongoUtilsPermission;


    @Qualifier("mongoTemplateSonar")
    private final MongoTemplate mongoTemplateSonar;


    public SonarKafkaConsumer(SonarService sonarService,
                              MongoUtils<PermissionsDbo> mongoUtilsPermission,
                              MongoTemplate mongoTemplateSonar) {
        this.sonarService = sonarService;
        this.mongoUtilsPermission = mongoUtilsPermission;
        this.mongoTemplateSonar = mongoTemplateSonar;
    }

    @KafkaListener(topics = SONAR_TOPIC, groupId = "sonar.consumer")
    public void consume(ConsumerRecord<Long, SonarConfig> record) {
        if (record.value() != null) {
            LOGGER.info("Kafka >> Received new Sonar Configuration {}", record.key());
            try {
                consumeRecord(record);
            } catch (Exception e) {
                LOGGER.error("Kafka >> Error Processing Record");
                throw new RuntimeException(e);
            }
        } else {
            LOGGER.error("Kafka >> Invalid Record: null");
        }
    }

    private void consumeRecord(ConsumerRecord<Long, SonarConfig> record) {
		LOGGER.info("SONAR >> Starting Configuration {}", new Timestamp(System.currentTimeMillis()));
        SonarConfig validatedPayload = validatePayload(record.value());
        dispatchUsers(validatedPayload.getSonarUsers());
        dispatchProjects(validatedPayload.getSonarProjects());
        dispatchPermissions(validatedPayload.getSonarPermissions());
        dispatchModifications(validatedPayload.getModifications());
		LOGGER.info("SONAR >> Configuration Finished {}", new Timestamp(System.currentTimeMillis()));
    }

    private SonarConfig validatePayload(SonarConfig payload) {
        return SonarConfig.builder()
                .sonarProjects(Objects.nonNull(payload.getSonarProjects()) ? payload.getSonarProjects() : Collections.emptyList())
                .sonarUsers(Objects.nonNull(payload.getSonarUsers()) ? payload.getSonarUsers() : Collections.emptyList())
                .sonarPermissions(Objects.nonNull(payload.getSonarPermissions()) ? payload.getSonarPermissions() : Collections.emptyList())
                .modifications(validateModifications(payload.getModifications()))
                .build();
    }

    private ModificationsDto validateModifications(ModificationsDto modifications) {
        return Objects.nonNull(modifications) ?
                ModificationsDto.builder()
                        .moveUsers(Objects.nonNull(modifications.getMoveUsers()) ? modifications.getMoveUsers() : Collections.emptyList())
                        .removeUsers(Objects.nonNull(modifications.getRemoveUsers()) ? modifications.getRemoveUsers() : Collections.emptyList())
                        .build() :
                ModificationsDto.builder()
                        .moveUsers(Collections.emptyList())
                        .removeUsers(Collections.emptyList())
                        .build();
    }

    private void dispatchProjects(List<ProjectDto> projects) {
        projects.forEach(projectDto -> {
            try {
                sonarService.createSonarProject(
                        SonarProject.builder()
                                .name(projectDto.getName())
                                .key(projectDto.getKey())
                                .visibility(VisibilityLevel.valueOf(projectDto.getVisibility().toUpperCase()))
                                .build());
                LOGGER.info("Successfully created project {}", projectDto.getName());
            } catch (HttpException | AutomationException | MongoException e) {
                LOGGER.error("ERROR >> Failed to create project {}", projectDto.getName());
            }
            dispatchMembers(projectDto.getKey(), Objects.nonNull(projectDto.getProjectsUsers()) ? projectDto.getProjectsUsers() : Collections.emptyList());
        });
    }


    private void dispatchMembers(String projectKey, List<PermissionsDto> users) {
        users.forEach(userDto -> {
            try {
                sonarService.addPermissionToUser(
                        SonarPermission.builder()
                                .login(userDto.getLogin())
                                .projectKey(projectKey)
                                .permission(PermissionLevel.valueOf(userDto.getPermission().toUpperCase()))
                                .build()
                );
                LOGGER.info("Successfully created member {} on project {}",
                        userDto.getLogin(),
                        projectKey);
            } catch (Exception e) {
                LOGGER.error("ERROR >> Failed to add member {} to project {}", userDto.getLogin(), projectKey);
            }
        });
    }

    private void dispatchUsers(List<UserDto> users) {
        users.forEach(this::dispatchUser);
    }

    private void dispatchUser(UserDto user) {
        try {
            sonarService.createSonarUser(
                    DtoToDomainMapper.mapUsers(user)
            );
            LOGGER.info("Successfully created user {}", user.getLogin());
        } catch (HttpException | AutomationException | MongoException e) {
            LOGGER.error("ERROR >> Failed to creating user: {}", user.getLogin());
        }
    }

    private void dispatchPermissions(List<PermissionsDto> permissions) {
        permissions.forEach(permissionsDto -> {
            try {
                sonarService.addPermissionToUser(
                        SonarPermission.builder()
                                .login(permissionsDto.getLogin())
                                .projectKey(permissionsDto.getProjectKey())
                                .permission(PermissionLevel.valueOf(permissionsDto.getPermission().toUpperCase()))
                                .build()
                );
                LOGGER.info("Successfully added permission {} for user {} on repository {}",
                        permissionsDto.getPermission(),
                        permissionsDto.getLogin(),
                        permissionsDto.getProjectKey());
            } catch (HttpException | AutomationException | MongoException e) {
                LOGGER.error("Error adding permission {} to user {} on project {}",
                        permissionsDto.getPermission(),
                        permissionsDto.getLogin(),
                        permissionsDto.getProjectKey());
            }
        });
    }

    private void dispatchRemoveUsers(List<RemoveUsers> removeUsers) {
        removeUsers.forEach(remove -> {
            remove.getProjects().forEach(project -> {
                try {
                    removeUserFromProject(project, remove.getLogin());
                    LOGGER.info("Successfully removed user {} from {}",
                            remove.getLogin(),
                            project);
                } catch (Exception e) {
                    LOGGER.error("ERROR >> Failed to remove user {} from {}",
                            remove.getLogin(),
                            project);
                }
            });
        });
    }

    private void removeUserFromProject(String projectKey, String login) {
        try {
            List<PermissionsDbo> permissions = mongoUtilsPermission.findRecord(
                    new Query(Criteria.where("login").is(login).and("projectKey").is(projectKey)), mongoTemplateSonar, PermissionsDbo.class);
            permissions.forEach(permission -> {
                try {
                    sonarService.removePermissionFromUser(
                            SonarPermission.builder()
                                    .login(login)
                                    .projectKey(projectKey)
                                    .permission(PermissionLevel.valueOf(permission.getPermission().toUpperCase()))
                                    .build()
                    );
                    LOGGER.info("Successfully removed permission {} from user {} on {}",
                            permission.getPermission(),
                            permission.getLogin(),
                            permission.getProjectKey());
                } catch (HttpException | AutomationException | MongoException e) {
                    LOGGER.error("ERROR >> Failed to remove user {} from project {}", login, projectKey);
                }
            });
        } catch (MongoException e) {
            LOGGER.error("Error removing user {} from project {}", login, projectKey);
        }
    }

    private void dispatchMoveUsers(List<MoveUsers> moveUsers) {
        moveUsers.forEach(move -> {
            try {
                sonarService.addPermissionToUser(
                        SonarPermission.builder()
                                .login(move.getLogin())
                                .projectKey(move.getProjectNameTarget())
                                .permission(PermissionLevel.valueOf(move.getPermission().toUpperCase()))
                                .build()
                );
                if (Objects.nonNull(move.getProjectNameSource())) {
                    removeUserFromProject(move.getProjectNameSource(), move.getLogin());
                }
                LOGGER.info("Successfully moved user {} from {} to {}",
                        move.getLogin(),
                        move.getProjectNameSource(),
                        move.getProjectNameTarget());
            } catch (Exception e) {
                LOGGER.error("ERROR >> Failed to move user {} from {} to {}",
                        move.getLogin(),
                        move.getProjectNameSource(),
                        move.getProjectNameTarget());
            }
        });
    }

    private void dispatchModifications(ModificationsDto modifications) {
        dispatchMoveUsers(modifications.getMoveUsers());
        dispatchRemoveUsers(modifications.getRemoveUsers());
    }
}
