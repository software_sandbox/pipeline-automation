package com.tmdei.sonar.core.kafka.config;

import com.tmdei.sonar.core.kafka.deserializer.SonarConfigDeserializer;
import com.tmdei.sonar.domain.dto.SonarConfig;
import com.tmdei.utils.kafka.DefaultKafkaConsumer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;

@EnableKafka
@Configuration
public class KafkaConsumerConfig {

    private final DefaultKafkaConsumer<Long, SonarConfig> defaultKafkaConsumer;
    @Value(value = "${kafka.bootstrapAddress}")
    private String bootstrapAddress;
    @Value(value = "${kafka.groupId}")
    private String groupId;

    public KafkaConsumerConfig(DefaultKafkaConsumer<Long, SonarConfig> defaultKafkaConsumer) {
        this.defaultKafkaConsumer = defaultKafkaConsumer;
    }

    @Bean
    public ConsumerFactory<Long, SonarConfig> consumerFactory() {
        return this.defaultKafkaConsumer.consumerFactory(this.bootstrapAddress, this.groupId,
                LongDeserializer.class,
                SonarConfigDeserializer.class);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<Long, SonarConfig>
    kafkaListenerContainerFactory() {
        return defaultKafkaConsumer.kafkaListenerContainerFactory(consumerFactory());
    }

}
