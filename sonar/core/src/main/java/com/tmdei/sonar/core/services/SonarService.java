package com.tmdei.sonar.core.services;

import com.tmdei.sonar.core.secure.gitlab.GitLabAuth;
import com.tmdei.sonar.domain.SonarDomain;
import com.tmdei.sonar.domain.SonarPermission;
import com.tmdei.sonar.domain.SonarProject;
import com.tmdei.sonar.domain.SonarUser;
import com.tmdei.sonar.domain.database.PermissionsDbo;
import com.tmdei.sonar.domain.database.SonarProjectDbo;
import com.tmdei.sonar.domain.database.SonarUserDbo;
import com.tmdei.utils.exceptions.AutomationException;
import com.tmdei.utils.http.DefaultHttpClient;
import com.tmdei.utils.http.exceptions.HttpException;
import com.tmdei.utils.mapper.GenericMapper;
import com.tmdei.utils.mongo.MongoUtils;
import com.tmdei.utils.mongo.exceptions.MongoException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

import static com.tmdei.sonar.core.utils.Variables.ADD_PERMISSION_PATH;
import static com.tmdei.sonar.core.utils.Variables.ADD_URL_PARAMS;
import static com.tmdei.sonar.core.utils.Variables.CONCAT_URL_PARAMS;
import static com.tmdei.sonar.core.utils.Variables.CREATE_PATH;
import static com.tmdei.sonar.core.utils.Variables.CREATE_USERS_PATH;
import static com.tmdei.sonar.core.utils.Variables.EMAIL_URL;
import static com.tmdei.sonar.core.utils.Variables.ERROR;
import static com.tmdei.sonar.core.utils.Variables.EXTERNAL_IDENTITY;
import static com.tmdei.sonar.core.utils.Variables.EXTERNAL_PROVIDER;
import static com.tmdei.sonar.core.utils.Variables.GITLAB;
import static com.tmdei.sonar.core.utils.Variables.LOCAL_URL;
import static com.tmdei.sonar.core.utils.Variables.LOGIN_URL;
import static com.tmdei.sonar.core.utils.Variables.NAME_URL;
import static com.tmdei.sonar.core.utils.Variables.OK;
import static com.tmdei.sonar.core.utils.Variables.PERMISSION;
import static com.tmdei.sonar.core.utils.Variables.PROJECT;
import static com.tmdei.sonar.core.utils.Variables.PROJECT_KEY;
import static com.tmdei.sonar.core.utils.Variables.REMOVE_PERMISSION_PATH;
import static com.tmdei.sonar.core.utils.Variables.SEARCH_BY_PROJECT_STATUS;
import static com.tmdei.sonar.core.utils.Variables.SEARCH_PROJECTS;
import static com.tmdei.sonar.core.utils.Variables.TOKEN_URL;
import static com.tmdei.sonar.core.utils.Variables.UPDATE_USER_AUTH_PATH;
import static com.tmdei.sonar.core.utils.Variables.USER;
import static com.tmdei.sonar.core.utils.Variables.VISIBILITY_URL;
import static com.tmdei.utils.utils.MiscUtils.checkThrow;
import static com.tmdei.utils.utils.MiscUtils.concatUrl;
import static com.tmdei.utils.utils.MiscUtils.getJsonNodeAsString;
import static com.tmdei.utils.utils.MiscUtils.getJsonNodeAsStringListIndex;
import static com.tmdei.utils.utils.MiscUtils.handleResponseStatusCode;

@Service
public class SonarService {

    private final String url;

    private final String gitlabSearch;

    private final GitLabAuth gitLabAuth;

    private final DefaultHttpClient<SonarDomain> sonarHttpClient;

    private final HttpClient httpClient;

    private final GenericMapper<SonarProject> mapper;

    private final GenericMapper<SonarUser> userMapper;

    private final MongoUtils<SonarProjectDbo> mongoUtilsProjects;

    private final MongoUtils<SonarUserDbo> mongoUtilsUsers;

    private final MongoUtils<PermissionsDbo> mongoUtilsPermissions;

    @Qualifier("mongoTemplateSonar")
    private final MongoTemplate mongoTemplateSonar;


    public SonarService(DefaultHttpClient<SonarDomain> sonarHttpClient, @Value("${sonar.api.default.url}") String url,
                        GenericMapper<SonarProject> mapper,
                        HttpClient httpClient,
                        @Value("${gitlab.api.default.search}") String gitlabSearch,
                        GitLabAuth gitLabAuth,
                        GenericMapper<SonarUser> userMapper,
                        MongoUtils<SonarProjectDbo> mongoUtilsProjects,
                        MongoUtils<SonarUserDbo> mongoUtilsUsers,
                        MongoUtils<PermissionsDbo> mongoUtilsPermissions,
                        MongoTemplate mongoTemplateSonar) {
        this.sonarHttpClient = sonarHttpClient;
        this.url = url;
        this.mapper = mapper;
        this.httpClient = httpClient;
        this.gitlabSearch = gitlabSearch;
        this.gitLabAuth = gitLabAuth;
        this.userMapper = userMapper;
        this.mongoUtilsProjects = mongoUtilsProjects;
        this.mongoUtilsUsers = mongoUtilsUsers;
        this.mongoTemplateSonar = mongoTemplateSonar;
        this.mongoUtilsPermissions = mongoUtilsPermissions;
    }

    public SonarProject createSonarProject(SonarProject sonarProject) throws HttpException, AutomationException, MongoException {
        SonarProject result = this.mapper.parseResponseBody(getJsonNodeAsString(this.sonarPost(null, url,
                CREATE_PATH, ADD_URL_PARAMS, NAME_URL, sonarProject.getName(),
                CONCAT_URL_PARAMS, TOKEN_URL, sonarProject.getKey(),
                CONCAT_URL_PARAMS, VISIBILITY_URL, sonarProject.getVisibility().visibility), PROJECT), SonarProject.class);
        mongoUtilsProjects.insertRecord(new SonarProjectDbo().map(result),
                mongoTemplateSonar);
        return result;
    }

    public SonarUser createSonarUser(SonarUser sonarUser) throws HttpException, AutomationException, MongoException {
        String handledResponse = this.sonarPost(null, url, CREATE_USERS_PATH, ADD_URL_PARAMS, LOCAL_URL, sonarUser.isLocal(),
                CONCAT_URL_PARAMS, NAME_URL, sonarUser.getName(),
                CONCAT_URL_PARAMS, LOGIN_URL, sonarUser.getLogin(),
                CONCAT_URL_PARAMS, EMAIL_URL, sonarUser.getEmail());
        updateUserAuthGitLab(sonarUser);
        SonarUser result = this.userMapper.parseResponseBody(getJsonNodeAsString(handledResponse, USER), SonarUser.class);
        mongoUtilsUsers.insertRecord(new SonarUserDbo().map(result),
                mongoTemplateSonar);
        return result;
    }

    public void addPermissionToUser(SonarPermission sonarPermission) throws HttpException, AutomationException, MongoException {
        this.sonarPost(null, url, ADD_PERMISSION_PATH, ADD_URL_PARAMS, LOGIN_URL, sonarPermission.getLogin(),
                CONCAT_URL_PARAMS, PERMISSION, sonarPermission.getPermission().permission,
                CONCAT_URL_PARAMS, PROJECT_KEY, sonarPermission.getProjectKey());
        mongoUtilsPermissions.insertRecord(new PermissionsDbo().map(sonarPermission), mongoTemplateSonar);

    }

    public void removePermissionFromUser(SonarPermission sonarPermission) throws HttpException, AutomationException, MongoException {
        this.sonarPost(null, url, REMOVE_PERMISSION_PATH, ADD_URL_PARAMS, LOGIN_URL, sonarPermission.getLogin(),
                CONCAT_URL_PARAMS, PERMISSION, sonarPermission.getPermission().permission,
                CONCAT_URL_PARAMS, PROJECT_KEY, sonarPermission.getProjectKey());
        this.mongoUtilsPermissions.deleteRecord(new PermissionsDbo().map(sonarPermission), mongoTemplateSonar);
    }

    public List<SonarProject> getPassingProjects() throws HttpException, AutomationException {
        String handledResponse = this.sonarGet(url, SEARCH_PROJECTS, SEARCH_BY_PROJECT_STATUS, OK);
        return parseProjects(handledResponse);
    }

    public List<SonarProject> getFailingProjects() throws HttpException, AutomationException {
        String handledResponse = this.sonarGet(url, SEARCH_PROJECTS, SEARCH_BY_PROJECT_STATUS, ERROR);
        return parseProjects(handledResponse);
    }

    public List<SonarProject> getAllProjects() throws HttpException, AutomationException {
        String handledResponse = this.sonarGet(url, SEARCH_PROJECTS);
        return parseProjects(handledResponse);
    }

    /********************************************************************************************************************************************************************************
     *
     * GENERIC METHODS
     *
     *******************************************************************************************************************************************************************************/
    private String sonarGet(Object... strings) throws HttpException, AutomationException {
        HttpResponse<String> response = this.sonarHttpClient.genericGetRequest(
                concatUrl(strings));
        String handledResponse = handleResponseStatusCode(response);
        checkThrow(response, handledResponse);
        return handledResponse;
    }

    private String sonarPost(SonarDomain body, Object... urlParams) throws HttpException, AutomationException {
        HttpResponse<String> response = this.sonarHttpClient.genericPostRequest(
                concatUrl(urlParams),
                body);
        String handledResponse = handleResponseStatusCode(response);
        checkThrow(response, handledResponse);
        return handledResponse;
    }

    /********************************************************************************************************************************************************************************
     *
     * UTILS METHODS
     *
     *******************************************************************************************************************************************************************************/

    private void updateUserAuthGitLab(SonarUser sonarUser) throws HttpException, AutomationException {
        HttpResponse<String> response = this.sonarHttpClient.genericPostRequest(
                concatUrl(url, UPDATE_USER_AUTH_PATH, ADD_URL_PARAMS, LOGIN_URL, sonarUser.getLogin(),
                        CONCAT_URL_PARAMS, EXTERNAL_PROVIDER, GITLAB,
                        CONCAT_URL_PARAMS, EXTERNAL_IDENTITY, getGitlabUserIdByUsername(sonarUser.getLogin())),
                null);
        String handledResponse = handleResponseStatusCode(response);
        checkThrow(response, handledResponse);
    }

    private Integer getGitlabUserIdByUsername(String username) throws AutomationException {
        try {
            HttpResponse<String> response = this.httpClient.send(HttpRequest.newBuilder()
                            .uri(URI.create(concatUrl(gitlabSearch, username)))
                            .headers(this.sonarHttpClient.buildHeaders(gitLabAuth.loadAuthHeaders()))
                            .GET()
                            .build(),
                    HttpResponse.BodyHandlers.ofString());
            String handledResponse = handleResponseStatusCode(response);
            System.out.println(response);
            checkThrow(response, handledResponse);
            return Integer.parseInt(getJsonNodeAsStringListIndex(handledResponse, "id", 0));
        } catch (Exception ex) {
            throw new AutomationException(ex.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    private List<SonarProject> parseProjects(String response) throws AutomationException {
        return this.mapper.parseResponseBodyList(
                getJsonNodeAsString(response, "components"), SonarProject.class);
    }
}
