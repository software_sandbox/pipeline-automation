package com.tmdei.gateway.services.repository;

import com.tmdei.repository.domain.Repository;
import com.tmdei.repository.domain.dto.RepositoryConfig;
import com.tmdei.utils.exceptions.AutomationException;
import com.tmdei.utils.http.DefaultHttpClient;
import com.tmdei.utils.http.exceptions.HttpException;
import com.tmdei.utils.mapper.GenericMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.net.http.HttpResponse;
import java.util.List;

import static com.tmdei.gateway.kafka.Topics.REPOSITORY_CONFIG_TOPIC;
import static com.tmdei.gateway.utils.Variables.ALL;
import static com.tmdei.utils.utils.MiscUtils.checkThrow;
import static com.tmdei.utils.utils.MiscUtils.concatUrl;
import static com.tmdei.utils.utils.MiscUtils.handleResponseStatusCode;
import static com.tmdei.utils.utils.MiscUtils.randomLong;

@Service
public class RepositoryService {

    private final DefaultHttpClient<Repository> repositoryHttpClient;

    private final String repositoryServiceUrl;


    private final GenericMapper<Repository> mapper;

    public RepositoryService(DefaultHttpClient<Repository> repositoryHttpClient,
                             @Value("${api.repository.url}") String repositoryServiceUrl,
                             GenericMapper<Repository> mapper) {
        this.repositoryHttpClient = repositoryHttpClient;
        this.repositoryServiceUrl = repositoryServiceUrl;
        this.mapper = mapper;
    }

    public List<Repository> getAllRepositories() throws AutomationException {
        try {
            return this.mapper.parseResponseBodyList(repositoryGet(repositoryServiceUrl, ALL), Repository.class);
        } catch (Exception e) {
            throw new AutomationException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    /********************************************************************************************************************************************************************************
     *
     * GENERIC METHODS
     *
     *******************************************************************************************************************************************************************************/
    private String repositoryGet(Object... strings) throws HttpException, AutomationException {
        HttpResponse<String> response = this.repositoryHttpClient.genericGetRequest(
                concatUrl(strings));
        String handledResponse = handleResponseStatusCode(response);
        checkThrow(response, handledResponse);
        return handledResponse;
    }
}
