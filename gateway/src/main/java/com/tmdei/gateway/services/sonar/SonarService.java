package com.tmdei.gateway.services.sonar;

import com.tmdei.sonar.domain.SonarDomain;
import com.tmdei.sonar.domain.SonarProject;
import com.tmdei.sonar.domain.dto.SonarConfig;
import com.tmdei.utils.exceptions.AutomationException;
import com.tmdei.utils.http.DefaultHttpClient;
import com.tmdei.utils.http.exceptions.HttpException;
import com.tmdei.utils.mapper.GenericMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.net.http.HttpResponse;
import java.util.List;

import static com.tmdei.gateway.kafka.Topics.SONAR_CONFIG_TOPIC;
import static com.tmdei.gateway.utils.Variables.SONAR_ALL;
import static com.tmdei.gateway.utils.Variables.SONAR_FAILING;
import static com.tmdei.gateway.utils.Variables.SONAR_PASSING;
import static com.tmdei.utils.utils.MiscUtils.checkThrow;
import static com.tmdei.utils.utils.MiscUtils.concatUrl;
import static com.tmdei.utils.utils.MiscUtils.handleResponseStatusCode;
import static com.tmdei.utils.utils.MiscUtils.randomLong;

@Service
public class SonarService {


    private final KafkaTemplate<Long, SonarConfig> sonarTemplate;

    private final DefaultHttpClient<SonarDomain> sonarHttpClient;

    private final GenericMapper<SonarProject> mapper;

    private final String sonarServiceUrl;

    public SonarService(KafkaTemplate<Long, SonarConfig> sonarTemplate,
                        DefaultHttpClient<SonarDomain> sonarHttpClient,
                        GenericMapper<SonarProject> mapper,
                        @Value("${api.sonar.url}") String sonarServiceUrl) {
        this.sonarTemplate = sonarTemplate;
        this.sonarServiceUrl = sonarServiceUrl;
        this.mapper = mapper;
        this.sonarHttpClient = sonarHttpClient;
    }

    public void dispatchSonarConfig(SonarConfig sonarConfig) throws AutomationException {
        try {
            this.sonarTemplate.send(SONAR_CONFIG_TOPIC, randomLong(), sonarConfig);
        } catch (Exception e) {
            throw new AutomationException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    public List<SonarProject> getPassingProjects() throws AutomationException {
        try {
            return this.mapper.parseResponseBodyList(sonarGet(sonarServiceUrl, SONAR_PASSING), SonarProject.class);
        } catch (Exception e) {
            throw new AutomationException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    public List<SonarProject> getFailingProjects() throws AutomationException {
        try {
            return this.mapper.parseResponseBodyList(sonarGet(sonarServiceUrl, SONAR_FAILING), SonarProject.class);
        } catch (Exception e) {
            throw new AutomationException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    public List<SonarProject> getAllProjects() throws AutomationException {
        try {
            return this.mapper.parseResponseBodyList(sonarGet(sonarServiceUrl, SONAR_ALL), SonarProject.class);
        } catch (Exception e) {
            throw new AutomationException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    /********************************************************************************************************************************************************************************
     *
     * GENERIC METHODS
     *
     *******************************************************************************************************************************************************************************/
    private String sonarGet(Object... strings) throws HttpException, AutomationException {
        HttpResponse<String> response = this.sonarHttpClient.genericGetRequest(
                concatUrl(strings));
        String handledResponse = handleResponseStatusCode(response);
        checkThrow(response, handledResponse);
        return handledResponse;
    }

    private String sonarPost(SonarDomain body, Object... urlParams) throws HttpException, AutomationException {
        HttpResponse<String> response = this.sonarHttpClient.genericPostRequest(
                concatUrl(urlParams),
                body);
        String handledResponse = handleResponseStatusCode(response);
        checkThrow(response, handledResponse);
        return handledResponse;
    }


}
