package com.tmdei.gateway.services.pipeline.mapper;

import com.tmdei.gateway.dto.PipelineConfig;
import com.tmdei.gateway.dto.PipelineModifications;
import com.tmdei.gateway.dto.PipelineUsers;
import com.tmdei.jenkins.domain.dto.JenkinsConfig;
import com.tmdei.jenkins.domain.dto.JobDto;
import com.tmdei.repository.domain.dto.RepositoryConfig;
import com.tmdei.repository.domain.dto.RepositoryDto;
import com.tmdei.repository.domain.dto.UserDto;
import com.tmdei.sonar.domain.dto.ProjectDto;
import com.tmdei.sonar.domain.dto.SonarConfig;

import java.util.ArrayList;
import java.util.List;

import static com.tmdei.utils.utils.MiscUtils.concatUrl;

public class ComponentsMapper {

    /********************************************************************************************************************************************************************************
     *
     * REPOSITORY
     *
     *******************************************************************************************************************************************************************************/
    public static RepositoryConfig mapRepository(PipelineConfig pipelineConfig) {
        return RepositoryConfig.builder()
                .repositories(List.of(
                        RepositoryDto.builder()
                                .default_branch("main")
                                .description(pipelineConfig.getDescription())
                                .name(pipelineConfig.getName())
                                .initialize_with_readme(true)
                                .visibility("private")
                                .users(mapRepositoryUsers(pipelineConfig.getUsers()))
                                .build()
                ))
                .users(mapRepositoryUsers(pipelineConfig.getUsers()))
                .build();
    }

    private static List<com.tmdei.repository.domain.dto.UserDto> mapRepositoryUsers(List<PipelineUsers> pipelineUsers) {
        List<UserDto> users = new ArrayList<>();
        pipelineUsers.forEach(user -> {
            users.add(
                    UserDto.builder()
                            .name(user.getName())
                            .access_level(user.getPermissions().getRepositoryPermission())
                            .reset_password(true)
                            .username(user.getUsername())
                            .email(user.getEmail())
                            .build()
            );
        });
        return users;
    }

    public static com.tmdei.repository.domain.dto.PermissionsDto mapRepositoryPermissionsChange(PipelineModifications pipelineModifications) {
        return
                com.tmdei.repository.domain.dto.PermissionsDto.builder()
                        .username(pipelineModifications.getPermissions().getUsername())
                        .projectName(pipelineModifications.getPermissions().getProject())
                        .permission(pipelineModifications.getPermissions().getPermissions().getRepositoryPermission())
                        .build();
    }

    public static com.tmdei.repository.domain.dto.ModificationsDto.RemoveUsers mapRepositoryRemoverUser(PipelineModifications pipelineModifications) {
        return
                com.tmdei.repository.domain.dto.ModificationsDto.RemoveUsers.builder()
                        .username(pipelineModifications.getRemoveUsers().getUsername())
                        .projects(pipelineModifications.getRemoveUsers().getPipelines())
                        .build();
    }

    public static com.tmdei.repository.domain.dto.ModificationsDto.MoveUsers mapRepositoryMoveUser(PipelineModifications pipelineModifications) {
        return
                com.tmdei.repository.domain.dto.ModificationsDto.MoveUsers.builder()
                        .username(pipelineModifications.getMoveUser().getUsername())
                        .permission(pipelineModifications.getMoveUser().getPermission().getRepositoryPermission())
                        .projectNameSource(pipelineModifications.getMoveUser().getPipelineSource())
                        .projectNameTarget(pipelineModifications.getMoveUser().getPipelineTarget())
                        .build();
    }

    /********************************************************************************************************************************************************************************
     *
     * JENKINS
     *
     *******************************************************************************************************************************************************************************/

    public static JenkinsConfig mapJenkins(PipelineConfig pipelineConfig, String defaultRepositoryUrl) {
        return JenkinsConfig.builder()
                .jobs(List.of(JobDto.builder()
                        .seedTemplate(pipelineConfig.getJenkinsTemplate())
                        .jobName(pipelineConfig.getName())
                        .repoUrl(concatUrl(defaultRepositoryUrl, pipelineConfig.getName(), ".git"))
                        .build()
                ))
                .build();
    }

    /********************************************************************************************************************************************************************************
     *
     * SONAR
     *
     *******************************************************************************************************************************************************************************/

    public static SonarConfig mapSonar(PipelineConfig pipelineConfig) {
        return SonarConfig.builder()
                .sonarUsers(mapSonarUsers(pipelineConfig.getUsers()))
                .sonarProjects(
                        List.of(
                                ProjectDto.builder()
                                        .key(pipelineConfig.getName())
                                        .name(pipelineConfig.getName())
                                        .visibility("private")
                                        .projectsUsers(mapSonarPermissions(pipelineConfig))
                                        .build()
                        )
                )
                .build();
    }

    private static List<com.tmdei.sonar.domain.dto.UserDto> mapSonarUsers(List<PipelineUsers> pipelineUsers) {
        List<com.tmdei.sonar.domain.dto.UserDto> users = new ArrayList<>();
        pipelineUsers.forEach(user -> {
            users.add(
                    com.tmdei.sonar.domain.dto.UserDto.builder()
                            .name(user.getName())
                            .login(user.getUsername())
                            .local(false)
                            .email(user.getEmail())
                            .build()
            );
        });
        return users;
    }

    private static List<com.tmdei.sonar.domain.dto.PermissionsDto> mapSonarPermissions(PipelineConfig pipelineConfig) {
        List<com.tmdei.sonar.domain.dto.PermissionsDto> permissions = new ArrayList<>();
        pipelineConfig.getUsers().forEach(user -> {
            permissions.add(
                    com.tmdei.sonar.domain.dto.PermissionsDto.builder()
                            .login(user.getUsername())
                            .permission(user.getPermissions().getSonarPermissions())
                            .projectKey(pipelineConfig.getName())
                            .build()
            );
        });
        return permissions;
    }

    public static com.tmdei.sonar.domain.dto.PermissionsDto mapSonarPermissionsChange(PipelineModifications pipelineModifications) {
        return
                com.tmdei.sonar.domain.dto.PermissionsDto.builder()
                        .login(pipelineModifications.getPermissions().getUsername())
                        .projectKey(pipelineModifications.getPermissions().getProject())
                        .permission(pipelineModifications.getPermissions().getPermissions().getSonarPermissions())
                        .build();
    }

    public static com.tmdei.sonar.domain.dto.ModificationsDto.RemoveUsers mapSonarRemoverUser(PipelineModifications pipelineModifications) {
        return
                com.tmdei.sonar.domain.dto.ModificationsDto.RemoveUsers.builder()
                        .login(pipelineModifications.getRemoveUsers().getUsername())
                        .projects(pipelineModifications.getRemoveUsers().getPipelines())
                        .build();
    }

    public static com.tmdei.sonar.domain.dto.ModificationsDto.MoveUsers mapSonarMoveUser(PipelineModifications pipelineModifications) {
        return
                com.tmdei.sonar.domain.dto.ModificationsDto.MoveUsers.builder()
                        .login(pipelineModifications.getMoveUser().getUsername())
                        .permission(pipelineModifications.getMoveUser().getPermission().getSonarPermissions())
                        .projectNameSource(pipelineModifications.getMoveUser().getPipelineSource())
                        .projectNameTarget(pipelineModifications.getMoveUser().getPipelineTarget())
                        .build();
    }


}
