package com.tmdei.gateway.services.pipeline;

import com.tmdei.gateway.database.GlobalPipelineConfigs;
import com.tmdei.gateway.dto.PipelineConfig;
import com.tmdei.gateway.dto.PipelineModifications;
import com.tmdei.gateway.services.pipeline.mapper.ComponentsMapper;
import com.tmdei.jenkins.domain.dto.JenkinsConfig;
import com.tmdei.repository.domain.dto.RepositoryConfig;
import com.tmdei.sonar.domain.dto.SonarConfig;
import com.tmdei.utils.exceptions.AutomationException;
import com.tmdei.utils.mongo.MongoUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import static com.tmdei.gateway.kafka.Topics.REPOSITORY_CONFIG_TOPIC;
import static com.tmdei.gateway.kafka.Topics.SONAR_CONFIG_TOPIC;
import static com.tmdei.utils.utils.MiscUtils.randomLong;

@Service
public class PipelineService {

    private final Logger LOGGER = LoggerFactory.getLogger(PipelineService.class);

    private final KafkaTemplate<Long, RepositoryConfig> repositoryTemplate;

    private final KafkaTemplate<Long, SonarConfig> sonarTemplate;

    private final KafkaTemplate<Long, JenkinsConfig> jenkinsTemplate;

    private final String defaultRepositoryUrl;

    private final MongoUtils<GlobalPipelineConfigs> mongoUtilsPipeline;


    @Qualifier("mongoTemplatePipeline")
    private final MongoTemplate mongoTemplatePipeline;

    public PipelineService(
            KafkaTemplate<Long, RepositoryConfig> repositoryTemplate,
            @Value("${gitlab.repositories.path}") String defaultRepositoryUrl,
            MongoUtils<GlobalPipelineConfigs> mongoUtilsPipeline,
            MongoTemplate mongoTemplatePipeline,
            KafkaTemplate<Long, SonarConfig> sonarTemplate,
            KafkaTemplate<Long, JenkinsConfig> jenkinsTemplate
    ) {
        this.repositoryTemplate = repositoryTemplate;
        this.defaultRepositoryUrl = defaultRepositoryUrl;
        this.mongoTemplatePipeline = mongoTemplatePipeline;
        this.mongoUtilsPipeline = mongoUtilsPipeline;
        this.sonarTemplate = sonarTemplate;
        this.jenkinsTemplate = jenkinsTemplate;
    }

    public void dispatchPipelines(List<PipelineConfig> pipelineConfigList) throws AutomationException {
        pipelineConfigList.forEach(config -> {
            try {
                dispatchPipelineConfig(config);
            } catch (AutomationException e) {
                LOGGER.error("ERROR >> Something went wrong while configuring the pipeline: {}", e.getLocalizedMessage());
            }
        });
    }

    private void dispatchPipelineConfig(PipelineConfig pipelineConfig) throws AutomationException {
        try {
            this.repositoryTemplate.send(REPOSITORY_CONFIG_TOPIC, randomLong(), ComponentsMapper.mapRepository(pipelineConfig));
            this.mongoUtilsPipeline.insertRecord(
                    new GlobalPipelineConfigs(
                            pipelineConfig.getName(),
                            ComponentsMapper.mapJenkins(pipelineConfig, defaultRepositoryUrl),
                            ComponentsMapper.mapSonar(pipelineConfig),
                            ComponentsMapper.mapRepository(pipelineConfig)
                    ), mongoTemplatePipeline);
        } catch (Exception e) {
            throw new AutomationException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    public void dispatchPipelineChanges(List<PipelineModifications> pipelineModifications) throws AutomationException {
        pipelineModifications.forEach(modification -> {
            try {
                if (Objects.nonNull(modification.getPermissions())) {
                    dispatchPipelinePermissionsChange(modification);
                }
                if (Objects.nonNull(modification.getRemoveUsers())) {
                    dispatchPipelineRemoveUser(modification);
                }
                if (Objects.nonNull(modification.getMoveUser())) {
                    dispatchPipelineMoveUser(modification);
                }
            } catch (Exception e) {
                LOGGER.error("ERROR >> Something went wrong while applying configuration changes to the pipeline: {}", e.getLocalizedMessage());
            }
        });
    }

    private void dispatchPipelinePermissionsChange(PipelineModifications pipelineModification) throws AutomationException {
        try {
            SonarConfig sonarConfig = SonarConfig.builder()
                    .sonarPermissions(List.of(ComponentsMapper.mapSonarPermissionsChange(pipelineModification)))
                    .build();
            RepositoryConfig repositoryConfig = RepositoryConfig.builder()
                    .permissions(List.of(ComponentsMapper.mapRepositoryPermissionsChange(pipelineModification)))
                    .build();
            this.sonarTemplate.send(SONAR_CONFIG_TOPIC, randomLong(),
                    sonarConfig);
            this.repositoryTemplate.send(REPOSITORY_CONFIG_TOPIC, randomLong(),
                    repositoryConfig);
            this.mongoUtilsPipeline.insertRecord(
                    new GlobalPipelineConfigs(
                            "PERMISSIONS CHANGED" + getCurrentDate(),
                            null,
                            sonarConfig,
                            repositoryConfig
                    ), mongoTemplatePipeline);
        } catch (Exception e) {
            throw new AutomationException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    private void dispatchPipelineRemoveUser(PipelineModifications pipelineModification) throws AutomationException {
        try {
            SonarConfig sonarConfig = SonarConfig.builder()
                    .modifications(
                            com.tmdei.sonar.domain.dto.ModificationsDto.builder()
                                    .removeUsers(List.of(ComponentsMapper.mapSonarRemoverUser(pipelineModification))
                                    )
                                    .build())
                    .build();
            RepositoryConfig repositoryConfig = RepositoryConfig.builder()
                    .modifications(
                            com.tmdei.repository.domain.dto.ModificationsDto.builder()
                                    .removeUsers(List.of(ComponentsMapper.mapRepositoryRemoverUser(pipelineModification))
                                    )
                                    .build())
                    .build();
            this.sonarTemplate.send(SONAR_CONFIG_TOPIC, randomLong(),
                    sonarConfig);
            this.repositoryTemplate.send(REPOSITORY_CONFIG_TOPIC, randomLong(),
                    repositoryConfig);
            this.mongoUtilsPipeline.insertRecord(
                    new GlobalPipelineConfigs(
                            "USER REMOVED" + getCurrentDate(),
                            null,
                            sonarConfig,
                            repositoryConfig
                    ), mongoTemplatePipeline);
        } catch (Exception e) {
            throw new AutomationException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    private void dispatchPipelineMoveUser(PipelineModifications pipelineModification) throws AutomationException {
        try {
            SonarConfig sonarConfig = SonarConfig.builder()
                    .modifications(
                            com.tmdei.sonar.domain.dto.ModificationsDto.builder()
                                    .moveUsers(List.of(ComponentsMapper.mapSonarMoveUser(pipelineModification))
                                    )
                                    .build())
                    .build();
            RepositoryConfig repositoryConfig = RepositoryConfig.builder()
                    .modifications(
                            com.tmdei.repository.domain.dto.ModificationsDto.builder()
                                    .moveUsers(List.of(ComponentsMapper.mapRepositoryMoveUser(pipelineModification))
                                    )
                                    .build())
                    .build();
            this.sonarTemplate.send(SONAR_CONFIG_TOPIC, randomLong(),
                    sonarConfig);
            this.repositoryTemplate.send(REPOSITORY_CONFIG_TOPIC, randomLong(),
                    repositoryConfig);
            this.mongoUtilsPipeline.insertRecord(
                    new GlobalPipelineConfigs(
                            "USER MOVED" + getCurrentDate(),
                            null,
                            sonarConfig,
                            repositoryConfig
                    ), mongoTemplatePipeline);
        } catch (Exception e) {
            throw new AutomationException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    private String getCurrentDate() {
        return LocalDateTime.now().toString();
    }


}
