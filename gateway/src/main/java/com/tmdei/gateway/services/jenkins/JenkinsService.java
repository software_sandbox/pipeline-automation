package com.tmdei.gateway.services.jenkins;

import com.tmdei.jenkins.domain.JobStatusInfo;
import com.tmdei.jenkins.domain.dto.JenkinsConfig;
import com.tmdei.jenkins.domain.dto.SeedDto;
import com.tmdei.utils.exceptions.AutomationException;
import com.tmdei.utils.http.DefaultHttpClient;
import com.tmdei.utils.http.exceptions.HttpException;
import com.tmdei.utils.mapper.GenericMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.net.http.HttpResponse;
import java.util.Collections;
import java.util.List;

import static com.tmdei.gateway.kafka.Topics.JENKINS_CONFIG_TOPIC;
import static com.tmdei.gateway.utils.Variables.JENKINS_JOB_STATUS;
import static com.tmdei.utils.utils.MiscUtils.checkThrow;
import static com.tmdei.utils.utils.MiscUtils.concatUrl;
import static com.tmdei.utils.utils.MiscUtils.handleResponseStatusCode;
import static com.tmdei.utils.utils.MiscUtils.randomLong;

@Service
public class JenkinsService {

    private final Logger LOGGER = LoggerFactory.getLogger(JenkinsService.class);
    private final KafkaTemplate<Long, JenkinsConfig> jenkinsTemplate;

    private final DefaultHttpClient<Object> jenkinsHttpClient;

    private final GenericMapper<JobStatusInfo> mapper;

    private final String jenkinsServiceUrl;

    public JenkinsService(KafkaTemplate<Long, JenkinsConfig> jenkinsTemplate,
                          DefaultHttpClient<Object> jenkinsHttpClient,
                          GenericMapper<JobStatusInfo> mapper,
                          @Value(value = "${api.jenkins.url}") String jenkinsServiceUrl) {
        this.jenkinsTemplate = jenkinsTemplate;
        this.jenkinsHttpClient = jenkinsHttpClient;
        this.mapper = mapper;
        this.jenkinsServiceUrl = jenkinsServiceUrl;
    }

    public List<JobStatusInfo> getJobsStatus() throws AutomationException {
        try {
            return this.mapper.parseResponseBodyList(jenkinsGet(jenkinsServiceUrl, JENKINS_JOB_STATUS), JobStatusInfo.class);
        } catch (Exception e) {
            throw new AutomationException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    public void triggerJenkinsJobs(List<String> jobs) throws AutomationException {
        try {
            this.jenkinsTemplate.send(JENKINS_CONFIG_TOPIC, randomLong(),
                    JenkinsConfig.builder()
                            .jobs(Collections.emptyList())
                            .trigger(jobs)
                            .seeds(Collections.emptyList())
                            .build());
        } catch (Exception e) {
            throw new AutomationException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    public void dispatchJenkinsConfig(List<SeedDto> jenkinsConfig) throws AutomationException {
        try {
            this.jenkinsTemplate.send(JENKINS_CONFIG_TOPIC, randomLong(),
                    JenkinsConfig.builder()
                            .jobs(Collections.emptyList())
                            .trigger(Collections.emptyList())
                            .seeds(jenkinsConfig)
                            .build());
        } catch (Exception e) {
            throw new AutomationException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    /********************************************************************************************************************************************************************************
     *
     * GENERIC METHODS
     *
     *******************************************************************************************************************************************************************************/
    private String jenkinsGet(Object... strings) throws HttpException, AutomationException {
        HttpResponse<String> response = this.jenkinsHttpClient.genericGetRequest(
                concatUrl(strings));
        String handledResponse = handleResponseStatusCode(response);
        checkThrow(response, handledResponse);
        return handledResponse;
    }
}
