package com.tmdei.gateway.controllers.pipeline;

import com.tmdei.gateway.dto.PipelineConfig;
import com.tmdei.gateway.dto.PipelineModifications;
import com.tmdei.gateway.services.pipeline.PipelineService;
import com.tmdei.utils.exceptions.AutomationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static com.tmdei.gateway.utils.Variables.APPLICATION_JSON;

@RestController
@RequestMapping(path = "/pipeline", produces = APPLICATION_JSON)
public class PipelineController {

    private final PipelineService pipelineService;


    public PipelineController(PipelineService pipelineService) {
        this.pipelineService = pipelineService;
    }

    @PostMapping(path = "/create")
    public void dispatchPipelineConfig(@RequestBody List<PipelineConfig> pipelineConfigList) {
        try {
            this.pipelineService.dispatchPipelines(pipelineConfigList);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        }
    }

    @PostMapping(path = "/update")
    public void dispatchPipelineChanges(@RequestBody List<PipelineModifications> pipelineModifications) {
        try {
            this.pipelineService.dispatchPipelineChanges(pipelineModifications);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        }
    }
}
