package com.tmdei.gateway.controllers.repository;

import com.tmdei.gateway.services.repository.RepositoryService;
import com.tmdei.repository.domain.Repository;
import com.tmdei.utils.exceptions.AutomationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static com.tmdei.gateway.utils.Variables.APPLICATION_JSON;


@RestController
@RequestMapping(path = "/repository", produces = APPLICATION_JSON)
public class RepositoryController {

    private final RepositoryService repositoryService;

    public RepositoryController(RepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }

    @GetMapping(path = "/all")
    public List<Repository> getAllRepositories() {
        try {
            return this.repositoryService.getAllRepositories();
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        }
    }
}
