package com.tmdei.gateway.controllers.sonar;

import com.tmdei.gateway.services.sonar.SonarService;
import com.tmdei.sonar.domain.SonarProject;
import com.tmdei.sonar.domain.dto.SonarConfig;
import com.tmdei.utils.exceptions.AutomationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static com.tmdei.gateway.utils.Variables.APPLICATION_JSON;

@RestController
@RequestMapping(path = "/sonar", produces = APPLICATION_JSON)
public class SonarController {

    private final SonarService sonarService;


    public SonarController(SonarService sonarService) {
        this.sonarService = sonarService;
    }

    @PostMapping
    public void dispatchSonarConfig(@RequestBody SonarConfig sonarConfig) {
        try {
            this.sonarService.dispatchSonarConfig(sonarConfig);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        }
    }

    @GetMapping(path = "/projects/all")
    public List<SonarProject> getAllProjects() {
        try {
            return this.sonarService.getAllProjects();
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        }
    }

    @GetMapping(path = "/projects/passing")
    public List<SonarProject> getPassingProjects() {
        try {
            return this.sonarService.getPassingProjects();
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        }
    }

    @GetMapping(path = "/projects/failing")
    public List<SonarProject> getFailingProjects() {
        try {
            return this.sonarService.getFailingProjects();
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        }
    }
}
