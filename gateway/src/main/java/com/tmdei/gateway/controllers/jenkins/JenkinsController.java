package com.tmdei.gateway.controllers.jenkins;

import com.tmdei.gateway.services.jenkins.JenkinsService;
import com.tmdei.jenkins.domain.JobStatusInfo;
import com.tmdei.jenkins.domain.dto.SeedDto;
import com.tmdei.utils.exceptions.AutomationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static com.tmdei.gateway.utils.Variables.APPLICATION_JSON;

@RestController
@RequestMapping(path = "/jenkins", produces = APPLICATION_JSON)
public class JenkinsController {

    private final JenkinsService jenkinsService;


    public JenkinsController(JenkinsService jenkinsService) {
        this.jenkinsService = jenkinsService;
    }

    @PostMapping(path = "/create/seed")
    public void dispatchJenkinsConfig(@RequestBody List<SeedDto> jenkinsConfig) {
        try {
            this.jenkinsService.dispatchJenkinsConfig(jenkinsConfig);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        }
    }

    @PostMapping(path = "/trigger/jobs")
    public void dispatchJenkinsTriggers(@RequestBody List<String> jobs) {
        try {
            this.jenkinsService.triggerJenkinsJobs(jobs);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        }
    }

    @GetMapping(path = "/jobs/status")
    public List<JobStatusInfo> getJobsStatus() {
        try {
            return this.jenkinsService.getJobsStatus();
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        }
    }
}
