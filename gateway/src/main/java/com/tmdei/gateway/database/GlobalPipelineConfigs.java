package com.tmdei.gateway.database;

import com.tmdei.jenkins.domain.dto.JenkinsConfig;
import com.tmdei.repository.domain.dto.RepositoryConfig;
import com.tmdei.sonar.domain.dto.SonarConfig;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document("pipeline.data")
public class GlobalPipelineConfigs {

    @Id
    private String name;

    private JenkinsConfig jenkinsConfig;

    private SonarConfig sonarConfig;

    private RepositoryConfig repositoryConfig;

}
