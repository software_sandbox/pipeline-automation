package com.tmdei.gateway.utils;


public class Variables {

    public static final String APPLICATION_JSON = "application/json";

    public static final String JENKINS_JOB_STATUS = "/status";

    public static final String SONAR_PASSING = "/projects/passing";

    public static final String SONAR_FAILING = "/projects/failing";

    public static final String SONAR_ALL = "/projects/all";

    public static final String ALL = "/all";


}
