package com.tmdei.gateway.kafka.config;

import com.tmdei.utils.kafka.DefaultKafkaConsumer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;

@EnableKafka
@Configuration
public class KafkaConsumerConfig {

    private final DefaultKafkaConsumer<Long, String> defaultKafkaConsumer;
    @Value(value = "${kafka.bootstrapAddress}")
    private String bootstrapAddress;
    @Value(value = "${kafka.groupId}")
    private String groupId;

    public KafkaConsumerConfig(DefaultKafkaConsumer<Long, String> defaultKafkaConsumer) {
        this.defaultKafkaConsumer = defaultKafkaConsumer;
    }

    @Bean
    public ConsumerFactory<Long, String> consumerFactory() {
        return this.defaultKafkaConsumer.consumerFactory(this.bootstrapAddress, this.groupId,
                LongDeserializer.class,
                StringDeserializer.class);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<Long, String>
    kafkaListenerContainerFactory() {
        return defaultKafkaConsumer.kafkaListenerContainerFactory(consumerFactory());
    }

}

