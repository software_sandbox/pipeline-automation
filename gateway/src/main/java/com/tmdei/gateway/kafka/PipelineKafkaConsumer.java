package com.tmdei.gateway.kafka;

import com.tmdei.gateway.database.GlobalPipelineConfigs;
import com.tmdei.jenkins.domain.dto.JenkinsConfig;
import com.tmdei.sonar.domain.dto.SonarConfig;
import com.tmdei.utils.mongo.MongoUtils;
import com.tmdei.utils.mongo.exceptions.MongoException;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

import static com.tmdei.gateway.kafka.Topics.JENKINS_CONFIG_TOPIC;
import static com.tmdei.gateway.kafka.Topics.PIPELINE_CONFIG_TOPIC;
import static com.tmdei.gateway.kafka.Topics.SONAR_CONFIG_TOPIC;
import static com.tmdei.utils.utils.MiscUtils.randomLong;

@Component
public class PipelineKafkaConsumer {

    private final Logger LOGGER = LoggerFactory.getLogger(PipelineKafkaConsumer.class);

    private final MongoUtils<GlobalPipelineConfigs> mongoUtilsPipeline;

    private final KafkaTemplate<Long, SonarConfig> sonarTemplate;

    private final KafkaTemplate<Long, JenkinsConfig> jenkinsTemplate;

    @Qualifier("mongoTemplatePipeline")
    private final MongoTemplate mongoTemplatePipeline;

    public PipelineKafkaConsumer(
            MongoUtils<GlobalPipelineConfigs> mongoUtilsPipeline,
            MongoTemplate mongoTemplatePipeline,
            KafkaTemplate<Long, SonarConfig> sonarTemplate,
            KafkaTemplate<Long, JenkinsConfig> jenkinsTemplate) {
        this.mongoUtilsPipeline = mongoUtilsPipeline;
        this.mongoTemplatePipeline = mongoTemplatePipeline;
        this.sonarTemplate = sonarTemplate;
        this.jenkinsTemplate = jenkinsTemplate;
    }

    @KafkaListener(topics = PIPELINE_CONFIG_TOPIC, groupId = "pipeline.consumer")
    public void consume(ConsumerRecord<Long, String> record) {
        if (record.value() != null) {
            LOGGER.info("KAFKA >> Received new Pipeline Configuration for {}", record.value());
            try {
                consumeRecord(record);
            } catch (Exception e) {
                LOGGER.error("KAFKA >> Error Processing Record");
                throw new RuntimeException(e);
            }
        } else {
            LOGGER.error("KAFKA >> Invalid Record: null");
        }
    }

    private void consumeRecord(ConsumerRecord<Long, String> record) {
        GlobalPipelineConfigs config = fetchConfig(record.value());
        if (Objects.nonNull(config)) {
            this.sonarTemplate.send(SONAR_CONFIG_TOPIC, randomLong(), config.getSonarConfig());
            this.jenkinsTemplate.send(JENKINS_CONFIG_TOPIC, randomLong(), config.getJenkinsConfig());
        } else {
            LOGGER.error("ERROR >> No Configuration Found for Group {}", record.value());
        }
    }

    private GlobalPipelineConfigs fetchConfig(String pipeline) {
        try {
            List<GlobalPipelineConfigs> globalPipelineConfigs = this.mongoUtilsPipeline.findRecord(
                    new Query(Criteria.where("_id").is(pipeline)), mongoTemplatePipeline, GlobalPipelineConfigs.class);
            return globalPipelineConfigs.isEmpty() ? null : globalPipelineConfigs.get(0);
        } catch (MongoException e) {
            LOGGER.error("MONGO >> Error Fetching Configuration from Database for {}", pipeline);
            return null;
        }
    }

}
