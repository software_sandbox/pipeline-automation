package com.tmdei.gateway.kafka;

public class Topics {

    public static final String REPOSITORY_CONFIG_TOPIC = "repository.config";

    public static final String SONAR_CONFIG_TOPIC = "sonar.config";

    public static final String JENKINS_CONFIG_TOPIC = "jenkins.config";

    public static final String PIPELINE_CONFIG_TOPIC = "pipeline.config";

}
