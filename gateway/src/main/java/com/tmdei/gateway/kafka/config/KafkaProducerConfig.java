package com.tmdei.gateway.kafka.config;

import com.tmdei.gateway.kafka.serializers.jenkins.JenkinsSerializer;
import com.tmdei.gateway.kafka.serializers.repository.RepositorySerializer;
import com.tmdei.gateway.kafka.serializers.sonar.SonarSerializer;
import com.tmdei.jenkins.domain.dto.JenkinsConfig;
import com.tmdei.repository.domain.dto.RepositoryConfig;
import com.tmdei.sonar.domain.dto.SonarConfig;
import com.tmdei.utils.kafka.DefaultKafkaProducer;
import org.apache.kafka.common.serialization.LongSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

@EnableKafka
@Configuration
public class KafkaProducerConfig {

    private final DefaultKafkaProducer<Long, RepositoryConfig> repositoryProducer;

    private final DefaultKafkaProducer<Long, SonarConfig> sonarTemplate;

    private final DefaultKafkaProducer<Long, JenkinsConfig> jenkinsTemplate;


    @Value(value = "${kafka.bootstrapAddress}")
    private String bootstrapAddress;


    public KafkaProducerConfig(DefaultKafkaProducer<Long, JenkinsConfig> jenkinsTemplate,
                               DefaultKafkaProducer<Long, RepositoryConfig> repositoryProducer,
                               DefaultKafkaProducer<Long, SonarConfig> sonarTemplate) {
        this.repositoryProducer = repositoryProducer;
        this.jenkinsTemplate = jenkinsTemplate;
        this.sonarTemplate = sonarTemplate;
    }

    /********************************************************************************************************************************************************************************
     *
     * JENKINS CONFIGS
     *
     *******************************************************************************************************************************************************************************/
    @Bean
    public ProducerFactory<Long, JenkinsConfig> producerFactoryJenkins() {
        return this.jenkinsTemplate.producerFactory(this.bootstrapAddress,
                LongSerializer.class,
                JenkinsSerializer.class);
    }

    @Bean
    public KafkaTemplate<Long, JenkinsConfig>
    jenkinsTemplate() {
        return jenkinsTemplate.kafkaTemplate(producerFactoryJenkins());
    }

    /********************************************************************************************************************************************************************************
     *
     * REPOSITORY CONFIGS
     *
     *******************************************************************************************************************************************************************************/
    @Bean
    public ProducerFactory<Long, RepositoryConfig> producerFactoryRepository() {
        return this.repositoryProducer.producerFactory(this.bootstrapAddress,
                LongSerializer.class,
                RepositorySerializer.class);
    }

    @Bean
    public KafkaTemplate<Long, RepositoryConfig>
    repositoryTemplate() {
        return repositoryProducer.kafkaTemplate(producerFactoryRepository());
    }

    /********************************************************************************************************************************************************************************
     *
     * SONAR CONFIGS
     *
     *******************************************************************************************************************************************************************************/
    @Bean
    public ProducerFactory<Long, SonarConfig> producerFactorySonar() {
        return this.sonarTemplate.producerFactory(this.bootstrapAddress,
                LongSerializer.class,
                SonarSerializer.class);
    }

    @Bean
    public KafkaTemplate<Long, SonarConfig>
    sonarTemplate() {
        return sonarTemplate.kafkaTemplate(producerFactorySonar());
    }

}
