package com.tmdei.gateway.kafka.serializers.repository;

import com.tmdei.repository.domain.dto.RepositoryConfig;
import com.tmdei.utils.kafka.serializer.DefaultSerializer;

public class RepositorySerializer extends DefaultSerializer<RepositoryConfig> {
}
