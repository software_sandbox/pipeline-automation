package com.tmdei.gateway.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PipelineModifications {

    private MoveUser moveUser;

    private RemoveUser removeUsers;

    private UserPermissions permissions;

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class MoveUser {

        private String username;

        private String pipelineSource;

        private String pipelineTarget;

        private PipelinePermissions permission;

    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class RemoveUser {

        private String username;

        private List<String> pipelines;


    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class UserPermissions {

        private String username;

        private String project;

        private PipelinePermissions permissions;

    }

}
