package com.tmdei.gateway.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PipelineConfig {

    private String name;

    private String description;

    private String jenkinsTemplate;

    private String forkUrl;

    private List<PipelineUsers> users;

}
