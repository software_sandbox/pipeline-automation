package com.tmdei.gateway.config;

import com.tmdei.gateway.kafka.config.KafkaProducerConfig;
import com.tmdei.utils.http.config.HttpConfig;
import com.tmdei.gateway.secure.AuthConfig;
import com.tmdei.utils.kafka.config.KafkaAdminConfig;
import com.tmdei.utils.mapper.MapperConfig;
import com.tmdei.utils.mongo.config.MongoConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(
        {
                HttpConfig.class,
                MapperConfig.class,
				AuthConfig.class,
                KafkaAdminConfig.class,
                KafkaProducerConfig.class,
                MongoConfig.class
        }
)
public class Config {
}
