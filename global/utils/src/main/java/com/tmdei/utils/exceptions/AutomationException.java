package com.tmdei.utils.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class AutomationException extends Exception {

    @Getter
    @Setter
    private int httpStatus;

    public AutomationException(String errorMessage, int httpStatus) {
        super(errorMessage);
        this.httpStatus = httpStatus;
    }
}
