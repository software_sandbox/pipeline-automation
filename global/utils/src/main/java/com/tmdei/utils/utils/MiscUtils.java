package com.tmdei.utils.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmdei.utils.exceptions.AutomationException;
import org.springframework.http.HttpStatus;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Objects;
import java.util.Random;

public class MiscUtils {


    public static String concatUrl(Object... args) {
        StringBuilder builder = new StringBuilder();
        for (Object arg : args) {
            builder.append(arg);
        }
        return builder.toString();
    }

    public static String handleResponseStatusCode(HttpResponse<String> response) {
        if (response.statusCode() >= HttpStatus.OK.value() &&
                response.statusCode() < HttpStatus.MULTIPLE_CHOICES.value()) {
            return response.body();
        } else {
            return null;
        }
    }

    public static void checkThrow(HttpResponse<String> response, String handledResponse) throws AutomationException {
        if (Objects.isNull(handledResponse)) {
            throw new AutomationException(response.body(), response.statusCode());
        }
    }

    public static byte[] xmlFileReplaceValue(String filePath, String nodeTag, String property, String newContent) throws AutomationException {
        try {
            Document doc = DocumentBuilderFactory
                    .newInstance()
                    .newDocumentBuilder()
                    .parse(filePath);
            NodeList list = doc.getElementsByTagName(nodeTag)
                    .item(0)
                    .getChildNodes();
            for (int i = 0; i < list.getLength(); i++) {
                Node node = list.item(i);
                if (property.equals(node.getNodeName())) {
                    node.setTextContent(newContent);
                }
            }
            Transformer transformer = TransformerFactory
                    .newInstance()
                    .newTransformer();
            StreamResult res = new StreamResult();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            res.setOutputStream(out);
            transformer.transform(new DOMSource(doc), res);

            return new StreamSource
                    (new ByteArrayInputStream(
                            out.toByteArray()))
                    .getInputStream()
                    .readAllBytes();
        } catch (Exception e) {
            throw new AutomationException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.ordinal());
        }
    }

    public static String convertFileToBase64(String filePath) throws AutomationException {
        try {
            return Base64.getEncoder().encodeToString(
                    Files.readAllBytes(Paths.get(filePath))
            );
        } catch (IOException ex) {
            throw new AutomationException(ex.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.ordinal());
        }
    }

    public static String encodeToBase64(String data) {
        return Base64.getEncoder().encodeToString(
                data.getBytes(StandardCharsets.UTF_8));
    }

    public static String decodeFromBase64(String dataInBase64) {
        return new String(Base64.getDecoder().decode(
                dataInBase64));
    }

    public static String getJsonNodeAsString(String fullJson, String jsonNode) throws AutomationException {
        try {
            return new ObjectMapper().readTree(fullJson).get(jsonNode).toPrettyString();
        } catch (IOException ex) {
            throw new AutomationException(ex.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.ordinal());
        }
    }

    public static String getJsonNodeAsStringListIndex(String fullJson, String jsonNode, Integer index) throws AutomationException {
        try {
            return new ObjectMapper().readTree(fullJson).get(0).get(jsonNode).toPrettyString();
        } catch (IOException ex) {
            throw new AutomationException(ex.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.ordinal());
        }
    }

    public static Long randomLong() {
        return Math.abs(new Random().nextLong());
    }
}
