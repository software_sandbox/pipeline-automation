package com.tmdei.utils.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmdei.utils.exceptions.AutomationException;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GenericMapper<R> {

    @Getter
    private final ObjectMapper mapper;

    private static final Integer INTERNAL_ERROR = 500;

    public GenericMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public R parseResponseBody(String body, Class<R> type) throws AutomationException {
        try {
            return this.mapper.readValue(body, type);
        } catch (Exception ex) {
            throw new AutomationException(ex.getLocalizedMessage(), INTERNAL_ERROR);
        }
    }

    public List<R> parseResponseBodyList(String body, Class<R> type) throws AutomationException {
        try {
            return this.mapper.readValue(body, mapper.getTypeFactory().constructCollectionType(List.class, type));
        } catch (Exception ex) {
            throw new AutomationException(ex.getLocalizedMessage(), INTERNAL_ERROR);
        }
    }
}
