package com.tmdei.utils.mongo.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class MongoException extends Exception {

    @Getter
    @Setter
    private String details;

    public MongoException(String errorMessage, String details) {
        super(errorMessage);
        this.details = details;
    }
}
