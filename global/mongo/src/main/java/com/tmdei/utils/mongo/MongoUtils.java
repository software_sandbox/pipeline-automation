package com.tmdei.utils.mongo;

import com.tmdei.utils.mongo.exceptions.MongoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public class MongoUtils<R> {

    private final Logger LOGGER = LoggerFactory.getLogger(MongoUtils.class);

    public void insertRecord(R record, MongoTemplate template) throws MongoException {
        try {
            template.insert(record);
        } catch (DuplicateKeyException e) {
            LOGGER.error("DUPLICATED >> Error inserting record: {}", record);
            throw new MongoException(e.getLocalizedMessage(), e.getMessage());
        }
    }

    public void deleteRecord(R record, MongoTemplate template) throws MongoException {
        try {
            template.remove(record);
        } catch (Exception e) {
            LOGGER.error("ERROR >> Deleting record: {}", record);
            throw new MongoException(e.getLocalizedMessage(), e.getMessage());
        }
    }

    public List<R> findRecord(Query query, MongoTemplate template, Class<R> type) throws MongoException {
        try {
            return template.find(query, type);
        } catch (Exception e) {
            LOGGER.error("ERROR >> Searching record: {}", query);
            throw new MongoException(e.getLocalizedMessage(), e.getMessage());
        }
    }

    public List<R> findAll(MongoTemplate template, Class<R> type) throws MongoException {
        try {
            return template.findAll(type);
        } catch (Exception e) {
            LOGGER.error("ERROR >> Searching all records");
            throw new MongoException(e.getLocalizedMessage(), e.getMessage());
        }
    }

    public R updateRecord(MongoTemplate template, R record) throws MongoException {
        try {
            return template.save(record);
        } catch (Exception e) {
            LOGGER.error("ERROR >> Updating record: {}", record);
            throw new MongoException(e.getLocalizedMessage(), e.getMessage());
        }
    }
}
