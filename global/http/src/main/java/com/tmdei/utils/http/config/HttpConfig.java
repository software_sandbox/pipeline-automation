package com.tmdei.utils.http.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmdei.utils.http.DefaultHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.http.HttpClient;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Map;

@Configuration
public class HttpConfig {

    private static final long TIMEOUT = 5_000;

    @Bean
    HttpClient httpClient() {
        return HttpClient.newBuilder()
                .connectTimeout(Duration.of(TIMEOUT, ChronoUnit.MILLIS))
                .build();
    }

    @Bean
    DefaultHttpClient<?> repositoryHttpClient(ObjectMapper mapper, HttpClient defaultHttpClient,
                                              Map<String, String> headers) {
        return new DefaultHttpClient<>(mapper, defaultHttpClient, headers);
    }

}
