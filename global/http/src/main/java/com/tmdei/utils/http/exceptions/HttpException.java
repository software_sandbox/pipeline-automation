package com.tmdei.utils.http.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class HttpException extends Exception {


    @Getter
    @Setter
    private int httpStatus;

    public HttpException(String errorMessage, int httpStatus) {
        super(errorMessage);
        this.httpStatus = httpStatus;
    }
}
