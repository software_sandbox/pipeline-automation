package com.tmdei.utils.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmdei.utils.http.exceptions.HttpException;

import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandler;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

public class DefaultHttpClient<R> {

    private final ObjectMapper mapper;

    private final java.net.http.HttpClient httpClient;

    private final Map<String, String> headers;

    private static final Integer INTERNAL_ERROR = 500;

    public DefaultHttpClient(ObjectMapper mapper, java.net.http.HttpClient httpClient, Map<String, String> headers) {
        this.mapper = mapper;
        this.httpClient = httpClient;
        this.headers = headers;
    }

    public String[] buildHeaders(Map<String, String> headers) {
        return headers.entrySet().stream().map(pair ->
                        Arrays.asList(pair.getKey(), pair.getValue()))
                .flatMap(Collection::stream)
                .collect(Collectors.toList())
                .toArray(String[]::new);
    }

    public HttpResponse<String> genericGetRequest(String url) throws HttpException {
        try {
            return httpClient.send(
                    defaultRequest(url, this.headers).GET().build(),
                    stringBodyHandler());
        } catch (Exception ex) {
            throw new HttpException(ex.getLocalizedMessage(), INTERNAL_ERROR);
        }
    }

    public HttpResponse<String> genericDeleteRequest(String url) throws HttpException {
        try {
            return httpClient.send(
                    defaultRequest(url, this.headers).DELETE().build(),
                    stringBodyHandler());
        } catch (Exception ex) {
            throw new HttpException(ex.getLocalizedMessage(), INTERNAL_ERROR);
        }
    }

    public HttpResponse<String> genericPostRequest(String url, R object) throws HttpException {
        try {
            addContentTypeHeaderJSON();
            return httpClient.send(
                    defaultRequest(url, this.headers).POST(BodyPublishers.ofString(mapper.writeValueAsString(object))).build(),
                    stringBodyHandler());
        } catch (Exception ex) {
            throw new HttpException(ex.getLocalizedMessage(), INTERNAL_ERROR);
        }
    }

    public HttpResponse<String> genericPutRequest(String url, R object) throws HttpException {
        try {
            addContentTypeHeaderJSON();
            return httpClient.send(
                    defaultRequest(url, this.headers).PUT(BodyPublishers.ofString(mapper.writeValueAsString(object))).build(),
                    stringBodyHandler());
        } catch (Exception ex) {
            throw new HttpException(ex.getLocalizedMessage(), INTERNAL_ERROR);
        }
    }

    public HttpResponse<String> genericPostRequestFile(String url, byte[] file) throws
            HttpException {
        try {
            addContentTypeHeaderXML();
            return httpClient.send(
                    defaultRequest(url, this.headers).POST(BodyPublishers.ofByteArray(file)).build(),
                    stringBodyHandler());
        } catch (Exception ex) {
            throw new HttpException(ex.getLocalizedMessage(), INTERNAL_ERROR);
        }
    }


    /*******************************************************************************************************************
     *
     * PRIVATE METHODS
     *
     *******************************************************************************************************************/
    private HttpRequest.Builder defaultRequest(String url, Map<String, String> headers) {
        return HttpRequest.newBuilder()
                .uri(URI.create(url))
                .headers(buildHeaders(headers));
    }

    private BodyHandler<String> stringBodyHandler() {
        return BodyHandlers.ofString();
    }

    private BodyHandler<byte[]> byteArrayBodyHandler() {
        return BodyHandlers.ofByteArray();
    }

    private void addContentTypeHeaderJSON() {
        this.headers.put("Content-Type", "application/json");
    }

    private void addContentTypeHeaderXML() {
        this.headers.put("Content-Type", "application/xml");
    }
}
