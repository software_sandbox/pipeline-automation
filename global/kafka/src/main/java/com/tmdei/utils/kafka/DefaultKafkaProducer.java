package com.tmdei.utils.kafka;

import lombok.NoArgsConstructor;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
public class DefaultKafkaProducer<K, V> {


    public ProducerFactory<K, V> producerFactory(String bootstrapAddress, Class<?> keyDeserializerClass,
                                                 Class<?> valueDeserializerClass) {
        Map<String, Object> configProps = new HashMap<>();
        configProps.put(
                ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                bootstrapAddress);
        configProps.put(
                ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                keyDeserializerClass);
        configProps.put(
                ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                valueDeserializerClass);
        return new DefaultKafkaProducerFactory<>(configProps);
    }

    public KafkaTemplate<K, V> kafkaTemplate(ProducerFactory<K, V> producerFactory) {
        return new KafkaTemplate<>(producerFactory);
    }


}
