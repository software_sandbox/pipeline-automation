package com.tmdei.utils.kafka.config;

import com.tmdei.utils.kafka.DefaultKafkaConsumer;
import com.tmdei.utils.kafka.DefaultKafkaProducer;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaAdminConfig {

    @Value(value = "${kafka.bootstrapAddress}")
    private String bootstrapAddress;

    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, this.bootstrapAddress);
        return new KafkaAdmin(configs);
    }

    @Bean
    public DefaultKafkaConsumer<?, ?> defaultKafkaConsumer() {
        return new DefaultKafkaConsumer<>();
    }

    @Bean
    public DefaultKafkaProducer<?, ?> defaultKafkaProducer() {
        return new DefaultKafkaProducer<>();
    }

}
