package com.tmdei.utils.kafka.serializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Objects;

@NoArgsConstructor
public abstract class DefaultSerializer<R> implements Serializer<R> {

    private final ObjectMapper objectMapper = new ObjectMapper();


    @Override
    public byte[] serialize(String s, R data) {
        try {
            if (data == null) {
                System.out.println("Null received at serializing");
                return null;
            }
            System.out.println("Serializing...");
            return objectMapper.writeValueAsBytes(data);
        } catch (Exception e) {
            System.out.printf("Error when serializing %s to byte[]", Objects.requireNonNull(data).getClass());
            return null;
        }
    }
}
