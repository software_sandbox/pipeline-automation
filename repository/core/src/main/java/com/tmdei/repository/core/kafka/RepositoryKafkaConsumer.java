package com.tmdei.repository.core.kafka;


import com.tmdei.repository.core.services.gitlab.GitLabService;
import com.tmdei.repository.domain.Repository;
import com.tmdei.repository.domain.database.RepositoryDbo;
import com.tmdei.repository.domain.database.UserDbo;
import com.tmdei.repository.domain.dto.ModificationsDto;
import com.tmdei.repository.domain.dto.ModificationsDto.MoveUsers;
import com.tmdei.repository.domain.dto.ModificationsDto.RemoveUsers;
import com.tmdei.repository.domain.dto.PermissionsDto;
import com.tmdei.repository.domain.dto.RepositoryConfig;
import com.tmdei.repository.domain.dto.RepositoryDto;
import com.tmdei.repository.domain.dto.UserDto;
import com.tmdei.repository.domain.gitlab.RepositoryMember;
import com.tmdei.repository.domain.gitlab.enums.AccessLevel;
import com.tmdei.repository.domain.mapper.DtoToDomainMapper;
import com.tmdei.utils.exceptions.AutomationException;
import com.tmdei.utils.http.exceptions.HttpException;
import com.tmdei.utils.mongo.MongoUtils;
import com.tmdei.utils.mongo.exceptions.MongoException;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.sql.Timestamp; 

import static com.tmdei.repository.core.kafka.config.KafkaTopicConfig.PIPELINE_TOPIC;
import static com.tmdei.repository.core.kafka.config.KafkaTopicConfig.REPOSITORY_TOPIC;
import static com.tmdei.utils.utils.MiscUtils.randomLong;

@Component
public class RepositoryKafkaConsumer {

    private final Logger LOGGER = LoggerFactory.getLogger(RepositoryKafkaConsumer.class);

    private final GitLabService gitLabService;

    private final MongoUtils<RepositoryDbo> mongoUtilsRepository;

    private final MongoUtils<UserDbo> mongoUtilsUsers;

    @Qualifier("mongoTemplateRepository")
    private final MongoTemplate mongoTemplateRepository;

    private final KafkaTemplate<Long, String> pipelineTemplate;


    public RepositoryKafkaConsumer(GitLabService gitLabService,
                                   MongoUtils<RepositoryDbo> mongoUtilsRepository,
                                   MongoUtils<UserDbo> mongoUtilsUsers,
                                   MongoTemplate mongoTemplateRepository,
                                   KafkaTemplate<Long, String> pipelineTemplate) {
        this.gitLabService = gitLabService;
        this.mongoUtilsRepository = mongoUtilsRepository;
        this.mongoUtilsUsers = mongoUtilsUsers;
        this.mongoTemplateRepository = mongoTemplateRepository;
        this.pipelineTemplate = pipelineTemplate;
    }


    @KafkaListener(topics = REPOSITORY_TOPIC, groupId = "repository.consumer")
    public void consume(ConsumerRecord<Long, RepositoryConfig> record) {
        if (record.value() != null) {
            LOGGER.info("KAFKA >> Received new Repository Configuration {}", record.key());
            try {
                consumeRecord(record);
            } catch (Exception e) {
                LOGGER.error("KAFKA >> Error Processing Record");
            }
        } else {
            LOGGER.error("KAFKA >> Invalid Record: null");
        }
    }

    private void consumeRecord(ConsumerRecord<Long, RepositoryConfig> record) {
		LOGGER.info("REPOSITORY >> Starting Configuration {}", new Timestamp(System.currentTimeMillis()));
        RepositoryConfig validatedPayload = validatePayload(record.value());
        dispatchUsers(validatedPayload.getUsers());
        dispatchRepositories(validatedPayload.getRepositories());
        dispatchPermissions(validatedPayload.getPermissions());
        dispatchModifications(validatedPayload.getModifications());
		LOGGER.info("REPOSITORY >> Configuration Finished {}", new Timestamp(System.currentTimeMillis()));
    }

    private RepositoryConfig validatePayload(RepositoryConfig payload) {
        return RepositoryConfig.builder()
                .repositories(Objects.nonNull(payload.getRepositories()) ? payload.getRepositories() : Collections.emptyList())
                .users(Objects.nonNull(payload.getUsers()) ? payload.getUsers() : Collections.emptyList())
                .permissions(Objects.nonNull(payload.getPermissions()) ? payload.getPermissions() : Collections.emptyList())
                .modifications(validateModifications(payload.getModifications()))
                .build();
    }

    private ModificationsDto validateModifications(ModificationsDto modifications) {
        return Objects.nonNull(modifications) ?
                ModificationsDto.builder()
                        .moveUsers(Objects.nonNull(modifications.getMoveUsers()) ? modifications.getMoveUsers() : Collections.emptyList())
                        .removeUsers(Objects.nonNull(modifications.getRemoveUsers()) ? modifications.getRemoveUsers() : Collections.emptyList())
                        .build() :
                ModificationsDto.builder()
                        .moveUsers(Collections.emptyList())
                        .removeUsers(Collections.emptyList())
                        .build();
    }

    private void dispatchRepositories(List<RepositoryDto> repositories) {
        repositories.forEach(repositoryDto -> {
            try {
                Repository createdRepository = gitLabService.createRepository(DtoToDomainMapper.mapRepository(repositoryDto));
                LOGGER.info("Repository {} created successfully", repositoryDto.getName());
                dispatchMembers(createdRepository.getId(), Objects.nonNull(repositoryDto.getUsers()) ? repositoryDto.getUsers() : Collections.emptyList());
                this.pipelineTemplate.send(PIPELINE_TOPIC, randomLong(), repositoryDto.getName());
            } catch (HttpException | AutomationException | MongoException e) {
                LOGGER.error("ERROR >> Failed to configure repository: {}", repositoryDto.getName());
            }
        });
    }

    private void
    dispatchMembers(Integer projectId, List<UserDto> users) {
        users.forEach(userDto -> {
            try {
                UserDbo userDbo = findUserByUsername(userDto.getUsername());
                if (Objects.nonNull(userDbo)) {
                    gitLabService.addMemberToProject(
                            RepositoryMember.builder()
                                    .user_id(userDbo.getId())
                                    .access_level(AccessLevel.fromInteger(userDto.getAccess_level()))
                                    .build(),
                            projectId
                    );
                    LOGGER.info("Successfully created member {} on repository {}",
                            userDto.getUsername(),
                           projectId);
                } else {
                    LOGGER.error("ERROR >> Verify if the given user is valid: {}",
                            userDto.getUsername());
                }
            } catch (HttpException | AutomationException | MongoException e) {
                LOGGER.error("ERROR >> Failed to configure member: {}", userDto.getUsername());
            }
        });
    }

    private void dispatchUsers(List<UserDto> users) {
        users.forEach(this::dispatchUser);
    }

    private void dispatchUser(UserDto user) {
        try {
            gitLabService.addUser(
                    DtoToDomainMapper.mapUsers(user)
            );
            LOGGER.info("Successfully created user {}", user.getUsername());
        } catch (HttpException | AutomationException | MongoException e) {
            LOGGER.error("ERROR >> Failed to configure user: {}", user.getUsername());
        }
    }

    private void dispatchPermissions(List<PermissionsDto> permissions) {
        permissions.forEach(permissionsDto -> {
            try {
                UserDbo userDbo = findUserByUsername(permissionsDto.getUsername());
                RepositoryDbo repositoryDbo = findRepositoryByName(permissionsDto.getProjectName());
                if (Objects.nonNull(userDbo) && Objects.nonNull(repositoryDbo)) {
                    gitLabService.editMemberPermissionsOnProject(
                            userDbo.getId(),
                            repositoryDbo.getId(),
                            RepositoryMember.builder().access_level(AccessLevel.fromInteger(permissionsDto.getPermission())).build()
                    );
                    LOGGER.info("Successfully added permission {} for user {} on repository {}",
                            permissionsDto.getPermission(),
                            permissionsDto.getUsername(),
                            permissionsDto.getProjectName());
                } else {
                    LOGGER.error("ERROR >> Verify if the given user and project are valid: {} - {}",
                            permissionsDto.getUsername(), permissionsDto.getProjectName());
                }
            } catch (HttpException | AutomationException | MongoException e) {
                LOGGER.error("ERROR >> Failed to configure permission {} for user {} on repository {}",
                        permissionsDto.getPermission(),
                        permissionsDto.getUsername(),
                        permissionsDto.getProjectName());
            }
        });
    }

    private void dispatchModifications(ModificationsDto modifications) {
        dispatchMoveUsers(modifications.getMoveUsers());
        dispatchRemoveUsers(modifications.getRemoveUsers());
    }

    private void dispatchMoveUsers(List<MoveUsers> moveUsers) {
        moveUsers.forEach(move -> {
            try {
                UserDbo userDbo = findUserByUsername(move.getUsername());
                RepositoryDbo repositoryDbo = findRepositoryByName(move.getProjectNameTarget());
                if (Objects.nonNull(userDbo) && Objects.nonNull(repositoryDbo)) {
                    gitLabService.addMemberToProject(
                            RepositoryMember.builder()
                                    .user_id(userDbo.getId())
                                    .access_level(AccessLevel.fromInteger(move.getPermission()))
                                    .build(),
                            repositoryDbo.getId()
                    );
                    if (Objects.nonNull(move.getProjectNameSource())) {
                        removeUserFromProject(move.getProjectNameSource(), move.getUsername());
                    }
                    LOGGER.info("Successfully moved user {} from {} to {}",
                            move.getUsername(),
                            move.getProjectNameSource(),
                            move.getProjectNameTarget());
                } else {
                    LOGGER.error("ERROR >> Verify if the given user and project are valid: {} - {}",
                            move.getUsername(), move.getProjectNameTarget());
                }
            } catch (Exception e) {
                LOGGER.error("ERROR >> Failed to move user {} from {} to {}",
                        move.getUsername(),
                        move.getProjectNameSource(),
                        move.getProjectNameTarget());
            }
        });
    }

    private void dispatchRemoveUsers(List<RemoveUsers> removeUsers) {
        removeUsers.forEach(remove -> {
            remove.getProjects().forEach(project -> {
                try {
                    removeUserFromProject(project, remove.getUsername());
                    LOGGER.info("Successfully removed user {} from {}",
                            remove.getUsername(),
                            project);
                } catch (Exception e) {
                    LOGGER.error("ERROR >> Failed to remove user {} from {}",
                            remove.getUsername(),
                            project);
                }
            });
        });
    }

    private void removeUserFromProject(String projectName, String username) {
        try {
            UserDbo userDbo = findUserByUsername(username);
            RepositoryDbo repositoryDbo = findRepositoryByName(projectName);
            if (Objects.nonNull(userDbo) && Objects.nonNull(repositoryDbo)) {
                gitLabService.removeMemberFromProject(userDbo.getId(), repositoryDbo.getId());
                LOGGER.info("Successfully removed user {} from {}",
                        username,
                        projectName);
            } else {
                LOGGER.error("ERROR >> Verify if the given user and project are valid: {} - {}",
                        username, projectName);
            }
        } catch (HttpException | AutomationException | MongoException e) {
            LOGGER.error("ERROR >> Failed to remove user {} from {}",
                    username,
                    projectName);
        }
    }

    private UserDbo findUserByUsername(String username) throws MongoException {
        List<UserDbo> users = mongoUtilsUsers.findRecord(
                new Query(Criteria.where("username").is(username)), mongoTemplateRepository, UserDbo.class);
        return users.isEmpty() ? null : users.get(0);

    }

    private RepositoryDbo findRepositoryByName(String projectName) throws MongoException {
        List<RepositoryDbo> projects = mongoUtilsRepository.findRecord(
                new Query(Criteria.where("name").is(projectName)), mongoTemplateRepository, RepositoryDbo.class);
        return projects.isEmpty() ? null : projects.get(0);
    }

}
