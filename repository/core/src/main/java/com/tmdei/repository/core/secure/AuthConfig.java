package com.tmdei.repository.core.secure;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
public class AuthConfig {

    private final BearerAuth bearerAuth;

    public AuthConfig(BearerAuth bearerAuth) {
        this.bearerAuth = bearerAuth;
    }

    @Bean
    public Map<String, String> headers() {
        return bearerAuth.loadAuthHeaders();
    }
}
