package com.tmdei.repository.core.mongo.config;

import com.mongodb.client.MongoClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
public class MongoConfig {


    private final MongoClient mongoClient;

    public MongoConfig(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }

    @Bean(name = {"mongoTemplateRepository", "mongoTemplate"})
    public MongoTemplate mongoTemplateRepository() {
        return new MongoTemplate(mongoClient, "repository");
    }
}

