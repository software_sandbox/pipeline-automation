package com.tmdei.repository.core.kafka.deserializer.gitlab;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmdei.repository.domain.dto.RepositoryConfig;
import org.apache.kafka.common.serialization.Deserializer;

import java.nio.charset.StandardCharsets;

public class RepositoryConfigDeserializer implements Deserializer<RepositoryConfig> {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public RepositoryConfig deserialize(String s, byte[] bytes) {
        try {
            if (bytes == null) {
                System.out.println("Null received at deserializing");
                return null;
            }
            System.out.println("Deserializing...");
            return objectMapper.readValue(new String(bytes, StandardCharsets.UTF_8), RepositoryConfig.class);
        } catch (Exception e) {
            System.out.println("Error when deserializing byte[] to a valid Repository Configuration");
            return null;
        }
    }
}
