package com.tmdei.repository.core.kafka.config;

import com.tmdei.repository.core.kafka.deserializer.gitlab.RepositoryConfigDeserializer;
import com.tmdei.repository.domain.dto.RepositoryConfig;
import com.tmdei.utils.kafka.DefaultKafkaConsumer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;

@EnableKafka
@Configuration
public class KafkaConsumerConfig {

    private final DefaultKafkaConsumer<Long, RepositoryConfig> defaultKafkaConsumer;
    @Value(value = "${kafka.bootstrapAddress}")
    private String bootstrapAddress;
    @Value(value = "${kafka.groupId}")
    private String groupId;

    public KafkaConsumerConfig(DefaultKafkaConsumer<Long, RepositoryConfig> defaultKafkaConsumer) {
        this.defaultKafkaConsumer = defaultKafkaConsumer;
    }

    @Bean
    public ConsumerFactory<Long, RepositoryConfig> consumerFactory() {
        return this.defaultKafkaConsumer.consumerFactory(this.bootstrapAddress, this.groupId,
                LongDeserializer.class,
                RepositoryConfigDeserializer.class);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<Long, RepositoryConfig>
    kafkaListenerContainerFactory() {
        return defaultKafkaConsumer.kafkaListenerContainerFactory(consumerFactory());
    }

}
