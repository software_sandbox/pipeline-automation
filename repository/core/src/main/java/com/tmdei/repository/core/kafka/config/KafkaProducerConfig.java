package com.tmdei.repository.core.kafka.config;

import com.tmdei.utils.kafka.DefaultKafkaProducer;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

@EnableKafka
@Configuration
public class KafkaProducerConfig {

    private final DefaultKafkaProducer<Long, String> repositoryProducer;

    @Value(value = "${kafka.bootstrapAddress}")
    private String bootstrapAddress;


    public KafkaProducerConfig(DefaultKafkaProducer<Long, String> repositoryProducer) {
        this.repositoryProducer = repositoryProducer;
    }

    @Bean
    public ProducerFactory<Long, String> producerFactory() {
        return this.repositoryProducer.producerFactory(this.bootstrapAddress,
                LongSerializer.class,
                StringSerializer.class);
    }

    @Bean
    public KafkaTemplate<Long, String>
    template() {
        return repositoryProducer.kafkaTemplate(producerFactory());
    }
}
