package com.tmdei.repository.core.controllers;


import com.tmdei.repository.core.services.RepositoryService;
import com.tmdei.repository.domain.Member;
import com.tmdei.repository.domain.Repository;
import com.tmdei.repository.domain.User;
import com.tmdei.utils.exceptions.AutomationException;
import com.tmdei.utils.http.exceptions.HttpException;
import com.tmdei.utils.mongo.exceptions.MongoException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static com.tmdei.repository.core.utils.Variables.APPLICATION_JSON;

@RestController
@RequestMapping(path = "/repository", produces = APPLICATION_JSON)
public class RepositoryController {

    private final RepositoryService repositoryService;

    public RepositoryController(RepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }


    @GetMapping(path = "/search/{id}")
    public ResponseEntity<Repository> findRepository(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(repositoryService.getRepository(id), HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        }
    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<Repository>> findAllRepositories() {
        try {
            return new ResponseEntity<>(repositoryService.getAllRepositories(), HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        }
    }

    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<String> deleteRepository(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(repositoryService.deleteRepository(id), HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        } catch (MongoException me) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, me.getMessage(), me);
        }
    }

    @PostMapping(path = "/create")
    public ResponseEntity<Repository> createRepository(@RequestBody Repository gitLabRepository) {
        try {
            return new ResponseEntity<>(repositoryService.createRepository(gitLabRepository), HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        } catch (MongoException me) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, me.getMessage(), me);
        }
    }

    @PostMapping(path = "/jenkins/{id}")
    public ResponseEntity<String> addJenkinsFile(@PathVariable Integer id) {
        try {
            return new ResponseEntity<>(repositoryService.commitJenkinsFile(id), HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        }
    }

    @GetMapping(path = "/{projId}/members")
    public ResponseEntity<List<Member>> getProjectMembers(@PathVariable int projId) {
        try {
            return new ResponseEntity<>(repositoryService.getProjectMembers(projId), HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        }
    }

    @PostMapping(path = "/{projId}/add/member")
    public ResponseEntity<Member> addMemberToProject(@PathVariable int projId, @RequestBody Member gitLabMember) {
        try {
            return new ResponseEntity<>(repositoryService.addMemberToProject(gitLabMember, projId), HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        }
    }

    @PostMapping(path = "/add/user")
    public ResponseEntity<User> addUser(@RequestBody User user) {
        try {
            return new ResponseEntity<>(repositoryService.addUser(user), HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        } catch (MongoException me) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, me.getMessage(), me);
        }
    }

    @DeleteMapping(path = "/{projId}/remove/member/{memberId}")
    public ResponseEntity<String> removeMemberFromProject(@PathVariable int projId, @PathVariable int memberId) {
        try {
            repositoryService.removeMemberFromProject(memberId, projId);
            return new ResponseEntity<>("{\"message\": \"Member Successfully Removed!\"}", HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        }
    }

    @PutMapping(path = "/{projId}/edit/member/{memberId}")
    public ResponseEntity<Member> editMemberPermissionsOnProject(@PathVariable int projId, @PathVariable int memberId,
                                                                 @RequestBody Member gitLabMember) {
        try {
            return new ResponseEntity<>(repositoryService.editMemberPermissionsOnProject(memberId, projId, gitLabMember), HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        }
    }

    @GetMapping(path = "/user/{username}")
    public ResponseEntity<User> getUserByUsername(@PathVariable String username) {
        try {
            return new ResponseEntity<>(repositoryService.getUserByUsername(username), HttpStatus.OK);
        } catch (AutomationException ae) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(ae.getHttpStatus()), ae.getMessage(), ae);
        } catch (HttpException httpE) {
            throw new ResponseStatusException(
                    HttpStatus.valueOf(httpE.getHttpStatus()), httpE.getMessage(), httpE);
        }
    }
}
