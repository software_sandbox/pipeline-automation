package com.tmdei.repository.core.services;

import com.tmdei.repository.domain.Member;
import com.tmdei.repository.domain.Repository;
import com.tmdei.repository.domain.User;
import com.tmdei.utils.exceptions.AutomationException;
import com.tmdei.utils.http.exceptions.HttpException;
import com.tmdei.utils.mongo.exceptions.MongoException;

import java.util.List;

public interface RepositoryService {

    Repository createRepository(Repository repository) throws HttpException, AutomationException, MongoException;

    Repository getRepository(long repositoryId) throws HttpException, AutomationException;

    List<Repository> getAllRepositories() throws HttpException, AutomationException;

    String deleteRepository(long repositoryId) throws HttpException, AutomationException, MongoException;

    String commitJenkinsFile(Integer repositoryId) throws AutomationException, HttpException;

    String commitSonarFile(Integer repositoryId, String projectName) throws AutomationException, HttpException;

    List<Member> getProjectMembers(int projectId) throws HttpException, AutomationException;

    User addUser(User user) throws HttpException, AutomationException, MongoException;

    Member addMemberToProject(Member user, Integer projectId) throws HttpException, AutomationException;

    void removeMemberFromProject(Integer userId, Integer projectId) throws HttpException, AutomationException;

    Member editMemberPermissionsOnProject(Integer userId, Integer projectId, Member user) throws HttpException, AutomationException;

    User getUserByUsername(String username) throws HttpException, AutomationException;

}
