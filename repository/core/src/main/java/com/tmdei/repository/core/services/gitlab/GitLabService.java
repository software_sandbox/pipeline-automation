package com.tmdei.repository.core.services.gitlab;

import com.tmdei.repository.core.services.RepositoryService;
import com.tmdei.repository.domain.Member;
import com.tmdei.repository.domain.Repository;
import com.tmdei.repository.domain.RepositoryDomain;
import com.tmdei.repository.domain.User;
import com.tmdei.repository.domain.database.RepositoryDbo;
import com.tmdei.repository.domain.database.UserDbo;
import com.tmdei.repository.domain.gitlab.GitLabCommitAction;
import com.tmdei.repository.domain.gitlab.GitLabRepository;
import com.tmdei.repository.domain.gitlab.RepositoryCommit;
import com.tmdei.repository.domain.gitlab.RepositoryUser;
import com.tmdei.repository.domain.gitlab.enums.DefaultBranch;
import com.tmdei.utils.exceptions.AutomationException;
import com.tmdei.utils.http.DefaultHttpClient;
import com.tmdei.utils.http.exceptions.HttpException;
import com.tmdei.utils.mapper.GenericMapper;
import com.tmdei.utils.mongo.MongoUtils;
import com.tmdei.utils.mongo.exceptions.MongoException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;

import static com.tmdei.repository.core.utils.Variables.BASE_64;
import static com.tmdei.repository.core.utils.Variables.COMMITS_PATH;
import static com.tmdei.repository.core.utils.Variables.CREATE_ACTION;
import static com.tmdei.repository.core.utils.Variables.DEFAULT_JENKINS_FILE;
import static com.tmdei.repository.core.utils.Variables.GITLAB_SEARCH_USERNAME;
import static com.tmdei.repository.core.utils.Variables.JENKINS;
import static com.tmdei.repository.core.utils.Variables.JENKINS_FILE_COMMIT_MESSAGE;
import static com.tmdei.repository.core.utils.Variables.MEMBERS_PATH;
import static com.tmdei.repository.core.utils.Variables.NEW_LINE;
import static com.tmdei.repository.core.utils.Variables.OWNED_PROJECTS;
import static com.tmdei.repository.core.utils.Variables.PROJECTS_PATH;
import static com.tmdei.repository.core.utils.Variables.SONAR_COMMIT_MESSAGE;
import static com.tmdei.repository.core.utils.Variables.SONAR_FILE;
import static com.tmdei.repository.core.utils.Variables.SONAR_KEY;
import static com.tmdei.repository.core.utils.Variables.SONAR_NAME;
import static com.tmdei.repository.core.utils.Variables.USERS_PATH;
import static com.tmdei.repository.core.utils.Variables.USER_DIR;
import static com.tmdei.utils.utils.MiscUtils.checkThrow;
import static com.tmdei.utils.utils.MiscUtils.concatUrl;
import static com.tmdei.utils.utils.MiscUtils.convertFileToBase64;
import static com.tmdei.utils.utils.MiscUtils.handleResponseStatusCode;


@Service("gitlabService")
public class GitLabService implements RepositoryService {

    private final String url;

    private final DefaultHttpClient<RepositoryDomain> repositoryHttpClient;

    private final GenericMapper<Repository> mapperRepositories;

    private final GenericMapper<User> mapperUsers;

    private final GenericMapper<Member> mapperMembers;

    private final MongoUtils<RepositoryDbo> mongoUtilsRepository;

    private final MongoUtils<UserDbo> mongoUtilsUsers;

    @Qualifier("mongoTemplateRepository")
    private final MongoTemplate mongoTemplateRepository;
	
	private final String repositoriesPath;


    public GitLabService(DefaultHttpClient<RepositoryDomain> repositoryHttpClient, @Value("${gitlab.api.default.url}") String url,
                         GenericMapper<Repository> mapperRepositories,
                         GenericMapper<User> mapperUsers,
                         GenericMapper<Member> mapperMembers,
                         MongoUtils<RepositoryDbo> mongoUtilsRepository,
                         MongoUtils<UserDbo> mongoUtilsUsers,
                         MongoTemplate mongoTemplateRepository,
						 @Value("${gitlab.repositories.path}") String repositoriesPath) {
        this.repositoryHttpClient = repositoryHttpClient;
        this.url = url;
        this.mapperRepositories = mapperRepositories;
        this.mapperUsers = mapperUsers;
        this.mapperMembers = mapperMembers;
        this.mongoUtilsRepository = mongoUtilsRepository;
        this.mongoTemplateRepository = mongoTemplateRepository;
        this.mongoUtilsUsers = mongoUtilsUsers;
		this.repositoriesPath = repositoriesPath;
    }

    @Override
    public Repository createRepository(Repository repository) throws HttpException, AutomationException, MongoException {
        Repository result = this.mapperRepositories.parseResponseBody(
                this.repositoryPost(repository, url, PROJECTS_PATH), Repository.class);
        commitSonarFile(result.getId(), result.getName());
		((GitLabRepository) result).setWeb_url(concatUrl(repositoriesPath, result.getName()));
        mongoUtilsRepository.insertRecord(new RepositoryDbo().map((GitLabRepository) result),
                mongoTemplateRepository);
        return result;
    }

    @Override
    public Repository getRepository(long repositoryId) throws HttpException, AutomationException {
        return this.mapperRepositories.parseResponseBody(
                this.repositoryGet(url, PROJECTS_PATH, repositoryId), Repository.class);

    }

    @Override
    public List<Repository> getAllRepositories() throws HttpException, AutomationException {
        return this.mapperRepositories.parseResponseBodyList(
                this.repositoryGet(url, PROJECTS_PATH, OWNED_PROJECTS), Repository.class);
    }

    @Override
    public String deleteRepository(long repositoryId) throws HttpException, AutomationException, MongoException {
        mongoUtilsRepository.deleteRecord(
                mongoUtilsRepository.findRecord(new Query(Criteria.where("_id").is(repositoryId)), mongoTemplateRepository, RepositoryDbo.class).get(0),
                mongoTemplateRepository);
        return this.repositoryDelete(url, PROJECTS_PATH, repositoryId);
    }

    @Override
    public String commitJenkinsFile(Integer repositoryId) throws AutomationException, HttpException {
        return this.repositoryPost(getDefaultJenkinsFileCommit(), url, PROJECTS_PATH, repositoryId, COMMITS_PATH);
    }

    @Override
    public String commitSonarFile(Integer repositoryId, String projectName) throws AutomationException, HttpException {
        return this.repositoryPost(
                getDefaultSonarFileCommit(projectName), url, PROJECTS_PATH, repositoryId, COMMITS_PATH);
    }

    @Override
    public List<Member> getProjectMembers(int projectId) throws HttpException, AutomationException {
        return mapperMembers.parseResponseBodyList(
                this.repositoryGet(url, PROJECTS_PATH, projectId, MEMBERS_PATH), Member.class);
    }

    @Override
    public User addUser(User gitLabUser) throws HttpException, AutomationException, MongoException {
        User result = mapperUsers.parseResponseBody(this.repositoryPost(gitLabUser, url, USERS_PATH), User.class);
        mongoUtilsUsers.insertRecord(new UserDbo().map((RepositoryUser) result),
                mongoTemplateRepository);
        return result;
    }

    @Override
    public Member addMemberToProject(Member gitLabMember, Integer projectId) throws HttpException, AutomationException {
        return mapperMembers.parseResponseBody(
                this.repositoryPost(gitLabMember, url, PROJECTS_PATH, projectId, MEMBERS_PATH), Member.class);
    }

    @Override
    public void removeMemberFromProject(Integer userId, Integer projectId) throws HttpException, AutomationException {
        this.repositoryDelete(url, PROJECTS_PATH, projectId, MEMBERS_PATH, userId);
    }

    @Override
    public Member editMemberPermissionsOnProject(Integer userId, Integer projectId, Member gitLabMember) throws HttpException, AutomationException {
        return mapperMembers.parseResponseBody(
                this.repositoryPut(gitLabMember, url, PROJECTS_PATH, projectId, MEMBERS_PATH, userId), Member.class);
    }

    /********************************************************************************************************************************************************************************
     *
     * PRIVATE METHODS
     *
     *      JENKINS RELATED
     *
     *      SONAR RELATED
     *******************************************************************************************************************************************************************************/
    private RepositoryCommit getDefaultJenkinsFileCommit() throws AutomationException {
        return RepositoryCommit.builder()
                .branch(DefaultBranch.MAIN.branch)
                .commit_message(JENKINS_FILE_COMMIT_MESSAGE)
                .actions(List.of(
                        GitLabCommitAction.builder()
                                .action(CREATE_ACTION)
                                .file_path(JENKINS)
                                .content(convertFileToBase64(System.getProperty(USER_DIR) + DEFAULT_JENKINS_FILE))
                                .encoding(BASE_64)
                                .build()
                ))
                .build();
    }

    private RepositoryCommit getDefaultSonarFileCommit(String projectName) throws AutomationException {
        return RepositoryCommit.builder()
                .branch(DefaultBranch.MAIN.branch)
                .commit_message(SONAR_COMMIT_MESSAGE)
                .actions(List.of(
                        GitLabCommitAction.builder()
                                .action(CREATE_ACTION)
                                .file_path(SONAR_FILE)
                                .content(generateDefaultSonarFile(projectName))
                                .encoding(BASE_64)
                                .build()
                ))
                .build();
    }

    private String generateDefaultSonarFile(String projectName) throws AutomationException {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            out.write(concatUrl(SONAR_KEY, projectName, NEW_LINE).getBytes(StandardCharsets.UTF_8));
            out.write(concatUrl(SONAR_NAME, projectName, NEW_LINE).getBytes(StandardCharsets.UTF_8));
            return Base64.getEncoder().encodeToString(out.toByteArray());
        } catch (IOException ex) {
            throw new AutomationException(ex.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    /********************************************************************************************************************************************************************************
     *
     * UTILS METHODS
     *
     *
     *      JENKINS RELATED
     *
     *      SONAR RELATED
     *
     *******************************************************************************************************************************************************************************/

    public User getUserByUsername(String username) throws HttpException, AutomationException {
        return mapperUsers.parseResponseBodyList(
                this.repositoryGet(url, GITLAB_SEARCH_USERNAME, username), User.class).get(0);
    }

    /********************************************************************************************************************************************************************************
     *
     * GENERIC METHODS
     *
     *******************************************************************************************************************************************************************************/
    private String repositoryGet(Object... strings) throws HttpException, AutomationException {
        HttpResponse<String> response = this.repositoryHttpClient.genericGetRequest(
                concatUrl(strings));
        String handledResponse = handleResponseStatusCode(response);
        checkThrow(response, handledResponse);
        return handledResponse;
    }

    private String repositoryDelete(Object... strings) throws HttpException, AutomationException {
        HttpResponse<String> response = this.repositoryHttpClient.genericDeleteRequest(
                concatUrl(strings));
        String handledResponse = handleResponseStatusCode(response);
        checkThrow(response, handledResponse);
        return handledResponse;
    }

    private String repositoryPost(RepositoryDomain body, Object... urlParams) throws HttpException, AutomationException {
        HttpResponse<String> response = this.repositoryHttpClient.genericPostRequest(
                concatUrl(urlParams),
                body);
        String handledResponse = handleResponseStatusCode(response);
        checkThrow(response, handledResponse);
        return handledResponse;
    }

    private String repositoryPut(RepositoryDomain body, Object... urlParams) throws HttpException, AutomationException {
        HttpResponse<String> response = this.repositoryHttpClient.genericPutRequest(
                concatUrl(urlParams),
                body);
        System.out.println(response);
        String handledResponse = handleResponseStatusCode(response);
        checkThrow(response, handledResponse);
        return handledResponse;
    }
}
