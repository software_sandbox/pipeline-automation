package com.tmdei.repository.core.kafka.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaTopicConfig {

    public static final String REPOSITORY_TOPIC = "repository.config";

    public static final String PIPELINE_TOPIC = "pipeline.config";

    private static final Integer NUM_PARTITIONS = 5;

    private static final Short REPLICATION_FACTOR = 1;

    @Bean
    public NewTopic repository() {
        return new NewTopic(REPOSITORY_TOPIC, NUM_PARTITIONS, REPLICATION_FACTOR);
    }

    @Bean
    public NewTopic pipeline() {
        return new NewTopic(PIPELINE_TOPIC, NUM_PARTITIONS, REPLICATION_FACTOR);
    }
}
