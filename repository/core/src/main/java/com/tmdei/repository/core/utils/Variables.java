package com.tmdei.repository.core.utils;

public class Variables {
    public static final String PROJECTS_PATH = "projects/";

    public static final String SONAR_KEY = "sonar.projectKey=";

    public static final String SONAR_NAME = "sonar.projectName=";

    public static final String NEW_LINE = "\n";

    public static final String SONAR_FILE = "sonar-project.properties";

    public static final String SONAR_COMMIT_MESSAGE = "Default Sonar Properties Added";

    public static final String MEMBERS_PATH = "/members/";

    public static final String USERS_PATH = "users/";

    public static final String OWNED_PROJECTS = "?owned=true";

    public static final String COMMITS_PATH = "/repository/commits";

    public static final String USER_DIR = "user.dir";

    public static final String DEFAULT_JENKINS_FILE = "\\core\\src\\main\\resources\\Jenkinsfile";

    public static final String JENKINS_FILE_COMMIT_MESSAGE = "Configure Jenkinsfile Template";

    public static final String CREATE_ACTION = "create";

    public static final String JENKINS = "Jenkinsfile";

    public static final String BASE_64 = "base64";

    public static final String GITLAB_SEARCH_USERNAME = "users?username=";

    public static final String APPLICATION_JSON = "application/json";

}
