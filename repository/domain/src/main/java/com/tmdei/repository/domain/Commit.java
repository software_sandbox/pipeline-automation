package com.tmdei.repository.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tmdei.repository.domain.gitlab.RepositoryCommit;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@JsonDeserialize(as = RepositoryCommit.class)
@JsonSerialize(as = RepositoryCommit.class)
public abstract class Commit implements RepositoryDomain {

    private String branch;

    private String commit_message;

}
