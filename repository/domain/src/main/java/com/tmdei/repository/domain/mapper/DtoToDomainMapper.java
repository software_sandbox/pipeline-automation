package com.tmdei.repository.domain.mapper;

import com.tmdei.repository.domain.Repository;
import com.tmdei.repository.domain.User;
import com.tmdei.repository.domain.dto.RepositoryDto;
import com.tmdei.repository.domain.dto.UserDto;
import com.tmdei.repository.domain.gitlab.GitLabRepository;
import com.tmdei.repository.domain.gitlab.RepositoryUser;
import com.tmdei.repository.domain.gitlab.enums.DefaultBranch;
import com.tmdei.repository.domain.gitlab.enums.VisibilityLevel;

public class DtoToDomainMapper {

    public static Repository mapRepository(RepositoryDto repositoryDto) {
        return GitLabRepository.builder()
                .name(repositoryDto.getName())
                .description(repositoryDto.getDescription())
                .default_branch(DefaultBranch.valueOf(repositoryDto.getDefault_branch().toUpperCase()))
                .initialize_with_readme(repositoryDto.isInitialize_with_readme())
                .visibility(VisibilityLevel.valueOf(repositoryDto.getVisibility().toUpperCase()))
                .build();
    }

    public static User mapUsers(UserDto userDto) {
        return RepositoryUser.builder()
                .name(userDto.getName())
                .username(userDto.getUsername())
                .email(userDto.getEmail())
                .reset_password(true)
                .build();
    }

}
