package com.tmdei.repository.domain.gitlab.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum AccessLevel {

    GUEST(10),
    REPORTER(20),
    DEVELOPER(30),
    MAINTAINER(40),
    OWNER(50);

    public final int level;

    private AccessLevel(int level) {
        this.level = level;
    }

    public static AccessLevel fromInteger(int x) {
        switch (x) {
            case 10:
                return GUEST;
            case 20:
                return REPORTER;
            case 30:
                return DEVELOPER;
            case 40:
                return MAINTAINER;
            case 50:
                return OWNER;
            default:
                return GUEST;
        }
    }

    @JsonValue
    int getLevel() {
        return level;
    }
}
