package com.tmdei.repository.domain.database;

import com.tmdei.repository.domain.gitlab.GitLabRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document("repositories.data")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RepositoryDbo {

    @Id
    private Integer id;

    @Indexed(unique = true)
    private String name;

    private String web_url;

    private boolean initialize_with_readme;

    private String default_branch;

    private String description;

    private String visibility;

    public RepositoryDbo map(GitLabRepository gitLabRepository) {
        return new RepositoryDbo(
                Objects.isNull(gitLabRepository.getId()) ? null : gitLabRepository.getId(),
                Objects.isNull(gitLabRepository.getName()) ? null : gitLabRepository.getName(),
                Objects.isNull(gitLabRepository.getWeb_url()) ? null : gitLabRepository.getWeb_url(),
                gitLabRepository.isInitialize_with_readme(),
                Objects.isNull(gitLabRepository.getDefault_branch()) ? null : gitLabRepository.getDefault_branch().branch,
                Objects.isNull(gitLabRepository.getDescription()) ? null : gitLabRepository.getDescription(),
                Objects.isNull(gitLabRepository.getVisibility()) ? null : gitLabRepository.getVisibility().level
        );
    }
}
