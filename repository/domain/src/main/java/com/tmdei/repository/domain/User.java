package com.tmdei.repository.domain;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tmdei.repository.domain.gitlab.RepositoryUser;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonDeserialize(as = RepositoryUser.class)
@JsonSerialize(as = RepositoryUser.class)
public abstract class User implements RepositoryDomain {

    private Integer id;

    private String name;
}
