package com.tmdei.repository.domain.gitlab;

import com.tmdei.repository.domain.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RepositoryUser extends User {

    private String username;

    private String name;

    private String state;

    private String web_url;

    private String email;

    private boolean reset_password = true;

}
