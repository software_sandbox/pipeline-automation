package com.tmdei.repository.domain.database;

import com.tmdei.repository.domain.gitlab.RepositoryUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document("users.data")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDbo {

    @Id
    private Integer id;

    private String name;

    @Indexed(unique = true)
    private String username;

    private String state;

    private String web_url;

    private String email;

    private boolean reset_password = true;

    public UserDbo map(RepositoryUser gitLabUser) {
        return new UserDbo(
                Objects.isNull(gitLabUser.getId()) ? null : gitLabUser.getId(),
                Objects.isNull(gitLabUser.getName()) ? null : gitLabUser.getName(),
                Objects.isNull(gitLabUser.getUsername()) ? null : gitLabUser.getUsername(),
                Objects.isNull(gitLabUser.getState()) ? null : gitLabUser.getState(),
                Objects.isNull(gitLabUser.getWeb_url()) ? null : gitLabUser.getWeb_url(),
                Objects.isNull(gitLabUser.getEmail()) ? null : gitLabUser.getEmail(),
                gitLabUser.isReset_password()
        );
    }
}
