package com.tmdei.repository.domain.gitlab.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum VisibilityLevel {
    @JsonProperty("private")
    PRIVATE("private"),
    @JsonProperty("internal")
    INTERNAL("internal"),
    @JsonProperty("public")
    PUBLIC("public");

    public final String level;

    private VisibilityLevel(String level) {
        this.level = level;
    }
}
