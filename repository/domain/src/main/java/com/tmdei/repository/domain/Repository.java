package com.tmdei.repository.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tmdei.repository.domain.gitlab.GitLabRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonDeserialize(as = GitLabRepository.class)
@JsonSerialize(as = GitLabRepository.class)
@SuperBuilder
public abstract class Repository implements RepositoryDomain {

    private Integer id;

    private String name;

}
