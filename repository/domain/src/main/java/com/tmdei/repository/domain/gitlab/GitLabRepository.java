package com.tmdei.repository.domain.gitlab;

import com.tmdei.repository.domain.Repository;
import com.tmdei.repository.domain.RepositoryDomain;
import com.tmdei.repository.domain.gitlab.enums.DefaultBranch;
import com.tmdei.repository.domain.gitlab.enums.VisibilityLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class GitLabRepository extends Repository implements RepositoryDomain {

    private String web_url;

    private boolean initialize_with_readme;

    private DefaultBranch default_branch;

    private String description;

    private VisibilityLevel visibility;


}
