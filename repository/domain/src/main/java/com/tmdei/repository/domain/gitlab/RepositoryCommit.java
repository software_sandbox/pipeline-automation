package com.tmdei.repository.domain.gitlab;

import com.tmdei.repository.domain.Commit;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@SuperBuilder
public class RepositoryCommit extends Commit {

    private List<GitLabCommitAction> actions;

}
