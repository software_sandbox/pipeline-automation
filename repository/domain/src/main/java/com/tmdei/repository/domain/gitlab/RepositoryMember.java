package com.tmdei.repository.domain.gitlab;

import com.tmdei.repository.domain.Member;
import com.tmdei.repository.domain.gitlab.enums.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RepositoryMember extends Member {

    private Integer user_id;

    private String username;

    private String name;

    private String state;

    private AccessLevel access_level;

    private String web_url;

}
