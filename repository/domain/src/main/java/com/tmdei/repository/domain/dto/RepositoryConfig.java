package com.tmdei.repository.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RepositoryConfig {

    private List<RepositoryDto> repositories;

    private List<PermissionsDto> permissions;

    private ModificationsDto modifications;

    private List<UserDto> users;

}
