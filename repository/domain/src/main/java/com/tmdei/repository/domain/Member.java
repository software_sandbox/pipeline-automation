package com.tmdei.repository.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tmdei.repository.domain.gitlab.RepositoryMember;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonDeserialize(as = RepositoryMember.class)
@JsonSerialize(as = RepositoryMember.class)
public abstract class Member implements RepositoryDomain {

    private Integer id;

    private String name;

}
