package com.tmdei.repository.domain.gitlab;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GitLabCommitAction {

    private String action;

    private String file_path;

    private String content;

    private String encoding;
}
