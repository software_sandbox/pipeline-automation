package com.tmdei.repository.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ModificationsDto {

    private List<MoveUsers> moveUsers;

    private List<RemoveUsers> removeUsers;

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class MoveUsers {

        private String username;

        private String projectNameSource;

        private String projectNameTarget;

        private Integer permission;

    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class RemoveUsers {

        private String username;

        private List<String> projects;


    }

}
