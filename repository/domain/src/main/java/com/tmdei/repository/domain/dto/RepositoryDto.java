package com.tmdei.repository.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RepositoryDto {

    private String name;

    private boolean initialize_with_readme;

    private String default_branch;

    private String description;

    private String visibility;

    private List<UserDto> users;

}
