package com.tmdei.repository.domain.gitlab.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum DefaultBranch {
    @JsonProperty("master")
    MASTER("master"),
    @JsonProperty("main")
    MAIN("main");

    public final String branch;

    DefaultBranch(String branch) {
        this.branch = branch;
    }
}
